﻿using LendFoundry.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Adapter
{
    public class AdapterService : IAdapterService
    {
        public AdapterService(
            ILogger logger,
            ITenantFieldRepository tenantFieldRepository,
            ITenantEnumRepository tenantEnumRepository,
            IMasterEnumRepository masterEnumRepository,
            IMasterFieldRepository masterFieldRepository)
        {
            Log = logger;
            TenantFieldRepository = tenantFieldRepository;
            TenantEnumRepository = tenantEnumRepository;
            MasterEnumRepository = masterEnumRepository;
            MasterFieldRepository = masterFieldRepository;
        }

        /// <summary>
        /// Logger
        /// </summary>
        private ILogger Log { get; }

        private ITenantFieldRepository TenantFieldRepository { get; }

        private ITenantEnumRepository TenantEnumRepository { get; }

        private IMasterEnumRepository MasterEnumRepository { get; }

        private IMasterFieldRepository MasterFieldRepository { get; }

        /// <summary>
        /// GetMasterField
        /// </summary>
        /// <param name="fieldname"></param>
        /// <returns></returns>
        public async Task<List<IMasterField>> GetMasterField(string fieldname)
        {
            Log.Debug("Started GetMasterField Service...");
            var result = await MasterFieldRepository.GetMasterFields(fieldname);
            Log.Debug("Ended GetMasterField Service...");
            if (!result.Any())
            {
                Log.Debug("No Record found in MasterFields...");
                throw new ArgumentException("No Record found in MasterFields...");
            }
            return result.ToList();
        }

        /// <summary>
        /// GetMasterEnum
        /// </summary>
        /// <param name="enumname"></param>
        /// <returns></returns>
        public async Task<List<IMasterEnum>> GetMasterEnum(string enumname)
        {
            Log.Debug("Started GetMasterEnum Service...");
            var result = await MasterEnumRepository.GetMasterEnums(enumname);
            Log.Debug("Ended GetMasterEnum Service...");
            if(!result.Any())
            {
                Log.Debug("No Record found in MasterEnums...");
                throw new ArgumentException("No Record found in MasterEnums...");
            }
            return result.ToList();
        }

        /// <summary>
        /// GetMasterEnumsByFieldName
        /// </summary>
        /// <param name="fieldname"></param>
        /// <returns></returns>
        public async Task<List<IMasterEnum>> GetMasterEnumsByFieldName(string fieldname)
        {
            Log.Debug("Started GetMasterEnumsByFieldName Service...");
            var result = await MasterEnumRepository.GetMasterEnumsByFieldName(fieldname);
            Log.Debug("Ended GetMasterEnumsByFieldName Service...");
            return result.ToList();
        }

        /// <summary>
        /// GetTenantField
        /// </summary>
        /// <param name="fieldname"></param>
        /// <returns></returns>
        public async Task<List<ITenantField>> GetTenantField(string fieldname)
        {
            Log.Debug("Started GetTenantField Service...");
            var result = await TenantFieldRepository.GetTenantFields(fieldname);
            Log.Debug("Ended GetTenantField Service...");
            if (!result.Any())
            {
                Log.Debug("No Record found in TenantFields...");
                throw new ArgumentException("No Record found in TenantFields...");
            }
            return result.ToList();
        }

        /// <summary>
        /// GetTenantEnum
        /// </summary>
        /// <param name="enumname"></param>
        /// <returns></returns>
        public async Task<List<ITenantEnum>> GetTenantEnum(string enumname)
        {
            Log.Debug("Started GetTenantEnum Service...");
            var result = await TenantEnumRepository.GetTenantEnums(enumname);
            Log.Debug("Ended GetTenantEnum Service...");
            if (!result.Any())
            {
                Log.Debug("No Record found in TenantEnums...");
                throw new ArgumentException("No Record found in TenantEnums...");
            }
            return result.ToList();
        }

        /// <summary>
        /// GetTenantEnumsByFieldName
        /// </summary>
        /// <param name="fieldname"></param>
        /// <returns></returns>
        public async Task<List<ITenantEnum>> GetTenantEnumsByFieldName(string fieldname)
        {
            Log.Debug("Started GetTenantEnumsByFieldName Service...");
            var result = await TenantEnumRepository.GetTenantEnumsByFieldName(fieldname);
            Log.Debug("Ended GetTenantEnumsByFieldName Service...");
            return result.ToList();
        }

        /// <summary>
        /// AddMasterField
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public async Task<IMasterField> AddMasterField(IMasterField field)
        {
            Log.Debug("Started AddMasterField Service...");
            var model = await MasterFieldRepository.GetMasterFields(field.StandardizedField);
            if (model.Any())
            {
                Log.Debug("Duplicate Record for MasterField...");
                throw new ArgumentException("Duplicate Record for MasterField...");
            }
            var result = await MasterFieldRepository.Add(field);
            Log.Debug("Ended AddMasterField Service...");
            return result;
        }

        /// <summary>
        /// AddMasterEnum
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<IMasterEnum> AddMasterEnum(IMasterEnum data)
        {
            Log.Debug("Started AddMasterEnum Service...");
            var field = await MasterFieldRepository.GetMasterFields(data.StandardizedField);
            if (!field.Any())
            {
                Log.Debug("No Record for MasterField...");
                throw new KeyNotFoundException("No Record for MasterField...");
            }
            var model = await MasterEnumRepository.GetMasterEnums(data.StandardizedEnum);
            if (model.Any())
            {
                Log.Debug("Duplicate Record for MasterEnum...");
                throw new ArgumentException("Duplicate Record for MasterEnum...");
            }
            var result = await MasterEnumRepository.Add(data);
            Log.Debug("Ended AddMasterEnum Service...");
            return result;
        }

        /// <summary>
        /// AddTenantField
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public async Task<ITenantField> AddTenantField(ITenantField field)
        {
            Log.Debug("Started AddTenantField Service...");
            var model = await TenantFieldRepository.GetTenantFields(field.StandardizedField);
            if (model.Any())
            {
                Log.Debug("Duplicate Record for TenantField...");
                throw new ArgumentException("Duplicate Record for TenantField...");
            }
            var result = await TenantFieldRepository.AddTenantField(field);
            Log.Debug("Ended AddTenantField Service...");
            return result;
        }

        /// <summary>
        /// AddTenantEnum
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ITenantEnum> AddTenantEnum(ITenantEnum data)
        {
            Log.Debug("Started AddTenantEnum Service...");

            Log.Debug("Check Tenant Field Records...");
            if (CheckFieldExists(data).Result == false)
            {
                Log.Debug("No Record for MasterField or TenantField...");
                throw new ArgumentException("No Record for MasterField or TenantField...");
            }

            var model = await TenantEnumRepository.GetTenantEnums(data.StandardizedEnum);
            if (model.Any())
            {
                Log.Debug("Duplicate Record for TenantEnum...");
                throw new ArgumentException("Duplicate Record for TenantEnum...");
            }
            var result = await TenantEnumRepository.AddTenantEnum(data);
            Log.Debug("Ended AddTenantEnum Service...");
            return result;
        }

        /// <summary>
        /// UpdateMasterField
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public async Task<IMasterField> UpdateMasterField(string fieldname, IMasterField field)
        {
            Log.Debug("Started UpdateMasterField Service...");
            var result = await MasterFieldRepository.Update(fieldname, field);
            Log.Debug("Ended UpdateMasterField Service...");
            return result;
        }

        /// <summary>
        /// UpdateMasterEnum
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<IMasterEnum> UpdateMasterEnum(string enumname, IMasterEnum data)
        {
            Log.Debug("Started UpdateMasterEnum Service...");
            var field = await MasterFieldRepository.GetMasterFields(data.StandardizedField);
            if (!field.Any())
            {
                Log.Debug("No Record for MasterField...");
                throw new KeyNotFoundException("No Record for MasterField...");
            }
            var result = await MasterEnumRepository.Update(enumname, data);
            Log.Debug("Ended UpdateMasterEnum Service...");
            return result;
        }

        /// <summary>
        /// UpdateTenantField
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public async Task<ITenantField> UpdateTenantField(string fieldname, ITenantField field)
        {
            Log.Debug("Started UpdateTenantField Service...");
            var result = await TenantFieldRepository.Update(fieldname, field);
            Log.Debug("Ended UpdateTenantField Service...");
            return result;
        }

        /// <summary>
        /// UpdateTenantEnum
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ITenantEnum> UpdateTenantEnum(string enumname, ITenantEnum data)
        {
            Log.Debug("Started UpdateTenantEnum Service...");
            if (CheckFieldExists(data).Result == false)
            {
                Log.Debug("No Record for MasterField or TenantField...");
                throw new ArgumentException("No Record for MasterField or TenantField...");
            }
            var result = await TenantEnumRepository.Update(enumname, data);
            Log.Debug("Ended UpdateTenantEnum Service...");
            return result;
        }

        /// <summary>
        /// DeleteMasterField
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMasterField(string fieldname)
        {
            Log.Debug("Started DeleteMasterField Service...");
            var result = await MasterFieldRepository.Delete(fieldname);
            Log.Debug("Ended DeleteMasterField Service...");
            return result;
        }

        /// <summary>
        /// DeleteMasterEnum
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMasterEnum(string enumname)
        {
            Log.Debug("Started DeleteMasterEnum Service...");
            var result = await MasterEnumRepository.Delete(enumname);
            Log.Debug("Ended DeleteMasterEnum Service...");
            return result;
        }

        /// <summary>
        /// DeleteTenantField
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public async Task<bool> DeleteTenantField(string fieldname)
        {
            Log.Debug("Started DeleteTenantField Service...");
            var result = await TenantFieldRepository.Delete(fieldname);
            Log.Debug("Ended DeleteTenantField Service...");
            return result;
        }

        /// <summary>
        /// DeleteTenantEnum
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<bool> DeleteTenantEnum(string enumname)
        {
            Log.Debug("Started DeleteTenantEnum Service...");
            var result = await TenantEnumRepository.Delete(enumname);
            Log.Debug("Ended DeleteTenantEnum Service...");
            return result;
        }

        /// <summary>
        /// GetTenantFieldDetail
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public async Task<List<ITenantFieldCustom>> GetTenantFieldDetail(string fieldName)
        {
            Log.Debug("Started GetTenantFieldDetail Service...");
            var tenantfields = await TenantFieldRepository.GetTenantFields(string.Empty);
            var tenantenums = await TenantEnumRepository.GetTenantEnums(string.Empty);
            var masterfields = await MasterFieldRepository.GetMasterFields(string.Empty);
            var masterenums = await MasterEnumRepository.GetMasterEnums(string.Empty);

            //ToDo: Need to check query for performance basis.
            var masterfieldenumlist = (from mf in masterfields
                                       join tf in tenantfields on mf.StandardizedField equals tf.StandardizedField into mff
                                       where (string.IsNullOrWhiteSpace(fieldName) ? true : mf.StandardizedField == fieldName)
                                       from mftemp in mff.DefaultIfEmpty()
                                       select new TenantFieldCustom
                                       {
                                           Id = mf.Id,
                                           StandardizedField = mf.StandardizedField,
                                           DefaultLabel = mf.Label,
                                           LastUpdatedBy = mf.LastUpdatedBy,
                                           LastUpdatedDate = mf.LastUpdatedDate,
                                           TenantFieldType = mf.Type,
                                           TenantId = (mftemp == null ? "" : mftemp.TenantId),
                                           TenantLabel = (mftemp == null ? mf.Label : ((mftemp.TenantLabel == string.Empty) ? mf.Label : mftemp.TenantLabel)),
                                           Enums = (from me in masterenums
                                                    join te in tenantenums on me.StandardizedEnum equals te.StandardizedEnum into mee
                                                    from metemp in mee.DefaultIfEmpty()
                                                    where me.StandardizedField == mf.StandardizedField
                                                    select new TenantEnumCustom
                                                    {
                                                        Id = me.Id,
                                                        StandardizedField = me.StandardizedField,
                                                        DefaultLabel = me.Label,
                                                        LastUpdatedBy = me.LastUpdatedBy,
                                                        LastUpdatedDate = me.LastUpdatedDate,
                                                        SequenceNo = me.SequenceNo,
                                                        StandardizedEnum = me.StandardizedEnum,
                                                        Visible = (metemp == null ? false : metemp.Visible),
                                                        TenantId = (metemp == null ? "" : metemp.TenantId),
                                                        TenantLabel = (metemp == null ? me.Label : ((metemp.TenantLabel == string.Empty) ? me.Label : metemp.TenantLabel))
                                                    }).ToList<ITenantEnumCustom>()
                                       }).ToList<ITenantFieldCustom>();

            ///Added Enums Which are only in Tenant Enum Table
            var uniqueTenantEnumList = tenantenums.Where(item => !masterenums.Any(item2 => item2.StandardizedEnum == item.StandardizedEnum));

            if (uniqueTenantEnumList.Any())
            {
                foreach (var mfe in masterfieldenumlist)
                {
                    foreach (var utenl in uniqueTenantEnumList)
                    {
                        if (mfe.StandardizedField == utenl.StandardizedField)
                        {
                            ITenantEnumCustom itfc = new TenantEnumCustom();
                            itfc.Id = utenl.Id;
                            itfc.StandardizedField = utenl.StandardizedField;
                            itfc.DefaultLabel = "ignore";
                            itfc.LastUpdatedBy = utenl.LastUpdatedBy;
                            itfc.LastUpdatedDate = utenl.LastUpdatedDate;
                            itfc.SequenceNo = utenl.SequenceNo;
                            itfc.StandardizedEnum = utenl.StandardizedEnum;
                            itfc.Visible = utenl.Visible;
                            itfc.TenantId = utenl.TenantId;
                            itfc.TenantLabel = utenl.TenantLabel;

                            mfe.Enums.Add(itfc);
                        }
                    }
                }
            }

            ///Added Fields that are only in Tenant Field Table
            var uniqueTenantFieldList = tenantfields.Where(item => !masterfields.Any(item2 => item2.StandardizedField == item.StandardizedField));

            if (uniqueTenantFieldList.Any())
            {
                var tenantfieldenumlist = (from tf in uniqueTenantFieldList
                                           where (string.IsNullOrWhiteSpace(fieldName) ? true : tf.StandardizedField == fieldName)
                                           select new TenantFieldCustom
                                           {
                                               Id = tf.Id,
                                               StandardizedField = tf.StandardizedField,
                                               DefaultLabel = "ignore",
                                               LastUpdatedBy = tf.LastUpdatedBy,
                                               LastUpdatedDate = tf.LastUpdatedDate,
                                               TenantFieldType = tf.TenantFieldType,
                                               TenantId = tf.TenantId,
                                               TenantLabel = tf.TenantLabel,
                                               Enums = (from te in tenantenums
                                                        where te.StandardizedField == tf.StandardizedField
                                                        select new TenantEnumCustom
                                                        {
                                                            Id = te.Id,
                                                            StandardizedField = te.StandardizedField,
                                                            DefaultLabel = "ignore",
                                                            LastUpdatedBy = te.LastUpdatedBy,
                                                            LastUpdatedDate = te.LastUpdatedDate,
                                                            SequenceNo = te.SequenceNo,
                                                            StandardizedEnum = te.StandardizedEnum,
                                                            Visible = te.Visible,
                                                            TenantId = te.TenantId,
                                                            TenantLabel = te.TenantLabel
                                                        }).ToList<ITenantEnumCustom>()
                                           }).ToList<ITenantFieldCustom>();

                masterfieldenumlist.AddRange(tenantfieldenumlist);
            }
            Log.Debug("Ended GetTenantFieldDetail Service...");
            return masterfieldenumlist;
        }

        /// <summary>
        /// GetTenantFieldDetail
        /// </summary>
        /// <returns></returns>
        public async Task<ITenantFieldEnumCustom> GetEnumFieldDetail()
        {
            Log.Debug("Started GetTenantFieldDetail Service...");
            var tenantfields = await TenantFieldRepository.GetTenantFields(string.Empty);
            var tenantenums = await TenantEnumRepository.GetTenantEnums(string.Empty);
            var masterfields = await MasterFieldRepository.GetMasterFields(string.Empty);
            var masterenums = await MasterEnumRepository.GetMasterEnums(string.Empty);

            ITenantFieldEnumCustom objTenantFieldEnumCustom = new TenantFieldEnumCustom();
            objTenantFieldEnumCustom.TenantFields = (from mf in masterfields
                                                     join tf in tenantfields on mf.StandardizedField equals tf.StandardizedField into mff
                                                     from mftemp in mff.DefaultIfEmpty()
                                                     select new TenantField
                                                     {
                                                         Id = mf.Id,
                                                         StandardizedField = mf.StandardizedField,
                                                         LastUpdatedBy = mf.LastUpdatedBy,
                                                         LastUpdatedDate = mf.LastUpdatedDate,
                                                         TenantFieldType = mf.Type,
                                                         TenantId = (mftemp == null ? "" : mftemp.TenantId),
                                                         TenantLabel = (mftemp == null ? mf.Label : ((mftemp.TenantLabel == string.Empty) ? mf.Label : mftemp.TenantLabel)),
                                                     }).ToList<ITenantField>();

            objTenantFieldEnumCustom.TenantEnums = (from me in masterenums
                                                    join te in tenantenums on me.StandardizedEnum equals te.StandardizedEnum into mee
                                                    from metemp in mee.DefaultIfEmpty()
                                                    select new TenantEnum
                                                    {
                                                        Id = me.Id,
                                                        StandardizedField = me.StandardizedField,
                                                        LastUpdatedBy = me.LastUpdatedBy,
                                                        LastUpdatedDate = me.LastUpdatedDate,
                                                        SequenceNo = me.SequenceNo,
                                                        StandardizedEnum = me.StandardizedEnum,
                                                        Visible = (metemp == null ? false : metemp.Visible),
                                                        TenantId = (metemp == null ? "" : metemp.TenantId),
                                                        TenantLabel = (metemp == null ? me.Label : ((metemp.TenantLabel == string.Empty) ? me.Label : metemp.TenantLabel))
                                                    }).ToList<ITenantEnum>();

            return objTenantFieldEnumCustom;
        }

        /// <summary>
        /// GetMasterFieldsByUpdateAfter give list of fields that are updated after
        /// </summary>
        /// <param name="updateAfterDate"></param>
        /// <returns></returns>
        public async Task<List<IMasterField>> GetMasterFieldsByUpdateAfter(DateTime updateAfterDate)
        {
            Log.Debug("Started GetMasterFieldsByUpdateAfter Service...");
            var result = await MasterFieldRepository.GetMasterFieldsByUpdateAfter(updateAfterDate);
            Log.Debug("Ended GetMasterFieldsByUpdateAfter Service...");
            return result.ToList();
        }

        /// <summary>
        /// GetMasterEnumsByUpdateAfter give list of Enums that are updated after
        /// </summary>
        /// <param name="updateAfterDate"></param>
        /// <returns></returns>
        public async Task<List<IMasterEnum>> GetMasterEnumsByUpdateAfter(DateTime updateAfterDate)
        {
            Log.Debug("Started GetMasterEnumsByUpdateAfter Service...");
            var result = await MasterEnumRepository.GetMasterEnumsByUpdateAfter(updateAfterDate);
            Log.Debug("Ended GetMasterEnumsByUpdateAfter Service...");
            return result.ToList();
        }

        /// <summary>
        /// GetTenantFieldsByUpdateAfter give list of fields that are updated after
        /// </summary>
        /// <param name="updateAfterDate"></param>
        /// <returns></returns>
        public async Task<List<ITenantField>> GetTenantFieldsByUpdateAfter(DateTime updateAfterDate)
        {
            Log.Debug("Started GetTenantFieldsByUpdateAfter Service...");
            var result = await TenantFieldRepository.GetTenantFieldsByUpdateAfter(updateAfterDate);
            Log.Debug("Ended GetTenantFieldsByUpdateAfter Service...");
            return result.ToList();
        }

        /// <summary>
        /// GetTenantEnumsByUpdateAfter give list of Enums that are updated after
        /// </summary>
        /// <param name="updateAfterDate"></param>
        /// <returns></returns>
        public async Task<List<ITenantEnum>> GetTenantEnumsByUpdateAfter(DateTime updateAfterDate)
        {
            Log.Debug("Started GetTenantEnumsByUpdateAfter Service...");
            var result = await TenantEnumRepository.GetTenantEnumsByUpdateAfter(updateAfterDate);
            Log.Debug("Ended GetTenantEnumsByUpdateAfter Service...");
            return result.ToList();
        }

        /// <summary>
        /// ImplementedCheckFieldExists Method to Check Whether Field Table in Tenant or Master
        /// contains data for selected enum
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private async Task<bool> CheckFieldExists(ITenantEnum data)
        {
            var tenantfield = await TenantFieldRepository.GetTenantFields(data.StandardizedField);
            if (!tenantfield.Any())
            {
                Log.Debug("Check Master Field Records...");
                var masterfield = await MasterFieldRepository.GetMasterFields(data.StandardizedField);
                if (!masterfield.Any())
                {
                    return false;
                }
                else
                {
                    var tenantFieldModel = new TenantField();
                    tenantFieldModel.StandardizedField = data.StandardizedField;
                    tenantFieldModel.TenantLabel = data.TenantLabel;

                    var masterFieldModel = masterfield.FirstOrDefault();
                    tenantFieldModel.TenantFieldType = masterFieldModel.Type;

                    await AddTenantField(tenantFieldModel);
                }
            }
            return true;
        }
    }
}