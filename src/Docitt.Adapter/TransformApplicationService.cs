﻿using Docitt.Application;
using Docitt.RequiredCondition;
using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Docitt.Questionnaire;

namespace Docitt.Adapter
{
    public class TransformApplicationService : ITransformApplicationService
    {
        public TransformApplicationService(ILogger logger,
            ITenantTime tenantTime,
            IApplicationService applicationService,
            IAdapterHelperService adapterHelperService,
            IRequiredConditionService requiredConditionService,
            IDocumentManagerService documentManagerService
            , IQuestionnaireService questionnaire
            )

        {
            if (logger == null) throw new ArgumentException($"{nameof(logger)} is mandatory");
            if (tenantTime == null) throw new ArgumentException($"{nameof(tenantTime)} is mandatory");
            if (applicationService == null) throw new ArgumentException($"{nameof(applicationService)} is mandatory");
            if (adapterHelperService == null) throw new ArgumentException($"{nameof(adapterHelperService)} is mandatory");
            if (requiredConditionService == null) throw new ArgumentException($"{nameof(requiredConditionService)} is mandatory");
            if (documentManagerService == null) throw new ArgumentException($"{nameof(documentManagerService)} is mandatory");
             if (questionnaire == null) throw new ArgumentException($"{nameof(questionnaire)} is mandatory");

            Logger = logger;
            TenantTime = tenantTime;
            ApplicationService = applicationService;
            AdapterHelperService = adapterHelperService;
            RequiredConditionService = requiredConditionService;
            DocumentManagerService = documentManagerService;
             QuestionnaireSection = questionnaire;
        }

        /// <summary>
        /// Gets Logger
        /// </summary>
        private ILogger Logger { get; }

        /// <summary>
        /// Gets TenantTime
        /// </summary>
        private ITenantTime TenantTime { get; }

        /// <summary>
        /// Gets AdapterHelperService
        /// </summary>
        private IAdapterHelperService AdapterHelperService { get; }

        /// <summary>
        /// Gets ApplicationService
        /// </summary>
        private IApplicationService ApplicationService { get; }

        private IRequiredConditionService RequiredConditionService { get; }

        private IDocumentManagerService DocumentManagerService { get; }

        /// <summary>
        /// Questionnaire Service
        /// </summary>
        private IQuestionnaireService QuestionnaireSection { get; }

        /// <summary>
        /// CheckIsApplicationSubmitted
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        public async Task<bool> CheckIsApplicationSubmitted(string applicationId)
        {
            try
            {
                var applicationResult = ApplicationService.GetByApplicationNumber(applicationId);

                if (applicationResult == null)
                    return await Task.Run(() => false); 
                else
                    return await Task.Run(() => true); 
            }
            catch (Exception ex)
            {
                Logger.Error($"The method CheckIsApplicationSubmitted({applicationId}) raised an error: {ex.Message}\n",ex);
                throw;
            }
        }

        /// <summary>
        /// GetLoanTransformData
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        public async Task<ITransformDataResponse> GetLoanTransformData(string applicationId)
        {
            try
            {
                var applicationResult = ApplicationService.GetByApplicationNumber(applicationId);

                if (applicationResult == null)
                    throw new NotFoundException($"Application not found for {applicationId} ");

                var questionnaireApplicationList = await QuestionnaireSection.GetBorrowersAndCoBorrowersAndSpouse(applicationId);
                var originalDataResponse = new LoanOriginalApplicationDataResponse(applicationResult,questionnaireApplicationList);
                
                var ruleResult = await AdapterHelperService.GetTransformRuleResult(originalDataResponse, applicationId);

                return ruleResult;
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetLoanTransformData({applicationId}) raised an error: {ex.Message}\n",ex);
                throw;
            }
        }

        /// <summary>
        /// GetConditionTransformData
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        public async Task<ITransformDataResponse> GetConditionTransformData(string applicationId)
        {
            try
            {
                var requiredConditionResult = await RequiredConditionService.GetRequestsByEntity("application", applicationId);

                if (requiredConditionResult == null || !requiredConditionResult.Any())
                    throw new NotFoundException($"Required condition not found for {applicationId} ");

                var originalDataResponse = requiredConditionResult.Select(p => new ConditionOriginalApplicationDataResponse(p)).ToList();

                var ruleResult = await AdapterHelperService.GetTransformRuleResult(originalDataResponse, applicationId);
                return ruleResult;
            }
            catch (NotFoundException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetConditionTransformData({applicationId}) raised an error: {ex.Message}\n",ex);
                throw;
            }
        }

        /// <summary>
        /// GetConditionDocumentsTransformData
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="conditionId"></param>
        /// <returns></returns>
        public async Task<ITransformDataResponse> GetConditionDocumentsTransformData(string applicationId, string conditionId)
        {
            try
            {
                var requiredConditionDocumentsResult = await RequiredConditionService.GetDocumentList("application", applicationId, conditionId);

                if (requiredConditionDocumentsResult == null || !requiredConditionDocumentsResult.Any())
                    throw new NotFoundException($"Required condition documents not found for {applicationId} ");

                //var originalDataResponse = requiredConditionDocumentsResult.Select(p => new ConditionDocumentsOriginalApplicationDataResponse(conditionId,p)).ToList();
                var originalDataResponse = requiredConditionDocumentsResult;

                var ruleResult = await AdapterHelperService.GetTransformRuleResult(originalDataResponse, applicationId);
                return ruleResult;
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetConditionDocumentsTransformData({applicationId},{conditionId}) raised an error: {ex.Message}\n",ex);
                throw;
            }
        }

        /// <summary>
        /// GetApplicationDocumentsTransformData
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        public async Task<ITransformDataResponse> GetApplicationDocumentsTransformData(string applicationId)
        {
            try
            {
                var applicationDocumentsResult = await DocumentManagerService.GetAll("application", applicationId);

                if (applicationDocumentsResult == null || !applicationDocumentsResult.Any())
                    throw new NotFoundException($"application documents not found for {applicationId} ");

                var originalDataResponse = applicationDocumentsResult.Select(p => new ApplicationDocumentsOriginalDataResponse(p)).ToList();

                var ruleResult = await AdapterHelperService.GetTransformRuleResult(originalDataResponse, applicationId);
                return ruleResult;
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetApplicationDocumentsTransformData({applicationId}) raised an error: {ex.Message}\n",ex);
                throw;
            }
        }

        /// <summary>
        /// GetLoanTransformData
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        public async Task<ITransformDataResponse> GetApplicationTransformData(string applicationId)
        {
            try
            {
                var applicationResult = ApplicationService.GetByApplicationNumber(applicationId);

                if (applicationResult == null)
                    throw new NotFoundException($"Application not found for {applicationId} ");
                var requiredConditionResult = await RequiredConditionService.GetRequestsByEntity("application", applicationId);

                var loanOriginalDataResponse = new LoanOriginalApplicationDataResponse(applicationResult);
                List<IConditionOriginalApplicationDataResponse> conditionOriginalDataResponse = null;
                if (requiredConditionResult != null)
                    conditionOriginalDataResponse = requiredConditionResult.Select(p => new ConditionOriginalApplicationDataResponse(p)).ToList<IConditionOriginalApplicationDataResponse>();

                var originalDataResponse = new ApplicationDetailedOriginalDataResponse()
                {
                    LoanApplication = loanOriginalDataResponse,
                    Conditions = conditionOriginalDataResponse
                };

                var ruleResult = await AdapterHelperService.GetTransformRuleResult(originalDataResponse, applicationId);

                return ruleResult;
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetApplicationTransformData({applicationId}) raised an error: {ex.Message}\n",ex);
                throw;
            }
        }
    }
}