namespace Docitt.Adapter.Extensions
{
    public static class StringExtensions
    {
        public static string ConvertToDocittCaseSensitiveType(this string param)
        {
            return param.ToLower();
        }
    }
}