﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Tenant.Client;
using System;
using System.Threading.Tasks;

namespace Docitt.Adapter
{
    public class AdapterHelperService : IAdapterHelperService
    {
        #region Constructor

        public AdapterHelperService(ILogger logger,
            ITenantTime tenantTime,
             ITenantService tenantService,
            IConfiguration configuration,
            IAdapterService adapterService,
            IDecisionEngineService decisionEngineService)
        {
            if (logger == null) throw new ArgumentException($"{nameof(logger)} is mandatory");
            if (tenantTime == null) throw new ArgumentException($"{nameof(tenantTime)} is mandatory");
            if (configuration == null) throw new ArgumentException($"{nameof(configuration)} is mandatory");
            if (adapterService == null) throw new ArgumentException($"{nameof(adapterService)} is mandatory");
            if (tenantService == null) throw new ArgumentException($"{nameof(tenantService)} is mandatory");

            Logger = logger;
            TenantTime = tenantTime;
            DecisionEngineService = decisionEngineService;
            Configuration = configuration;
            AdapterService = adapterService;
            TenantService = tenantService;
        }

        #endregion Constructor

        #region Variable Declaration

        /// <summary>
        /// Gets Logger
        /// </summary>
        private ILogger Logger { get; }

        /// <summary>
        /// Gets TenantTime
        /// </summary>
        private ITenantTime TenantTime { get; }

        /// <summary>
        /// Gets TenantService
        /// </summary>
        private ITenantService TenantService { get; }

        /// <summary>
        /// Gets DecisionEngineService
        /// </summary>
        private IDecisionEngineService DecisionEngineService { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Gets AdapterService
        /// </summary>
        private IAdapterService AdapterService { get; }

        #endregion Variable Declaration

        #region Methods

        public async Task<ITransformDataResponse> GetTransformRuleResult(dynamic originalData, string applicationId, string borrowerId = null)
        {
            try
            {
                Logger.Info($"GetTransformRuleResult start...");
                Logger.Info($"...Execute DecisionEngine({Configuration.TransformRule})");
                var masterResult = await AdapterService.GetEnumFieldDetail();
                var tenantId = TenantService.Current.Id;
                var payload = new RuleRequestPayload(originalData, masterResult, tenantId, applicationId);
                payload.BorrowerId = borrowerId;
                var computeResult = DecisionEngineService.Execute<dynamic, TransformDataResponse>(Configuration.TransformRule, new { payload = payload });
                Logger.Info($"...Executed");
                if (computeResult.Result == "Passed")
                    return computeResult;
                else
                    return null;
            }
            catch (Exception ex)
            {
                Logger.Error($"[ERROR] in the method GetTransformRuleResult : {ex}", ex);
                throw new ArgumentException($"{ex}",ex);
            }
        }

        #endregion Methods
    }
}