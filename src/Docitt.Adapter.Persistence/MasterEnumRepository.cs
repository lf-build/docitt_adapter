﻿using AutoMapper;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Docitt.Adapter.Persistence
{
    public class MasterEnumRepository : IMasterEnumRepository
    {
        static MasterEnumRepository()
        {
            BsonClassMap.RegisterClassMap<MasterEnum>(map =>
            {
                map.AutoMap();
            });
        }

        public MasterEnumRepository(IMongoConfiguration configuration, ITenantService tenantService)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (tenantService == null)
                throw new ArgumentNullException(nameof(tenantService));

            if (string.IsNullOrWhiteSpace(configuration.ConnectionString))
                throw new ArgumentException("Connection String cannot be empty");

            if (string.IsNullOrWhiteSpace(configuration.Database))
                throw new ArgumentException("Database name cannot be empty");

            TenantService = tenantService;

            var client = new MongoClient(configuration.ConnectionString);
            var database = client.GetDatabase(configuration.Database);
            Collection = database.GetCollection<MasterEnum>("masterenum");

            Collection.Indexes.CreateOneAsync(
                Builders<MasterEnum>.IndexKeys.Ascending(u => u.Id), new CreateIndexOptions { Unique = true, Name = "id" });

            Collection.Indexes.CreateOneAsync(
                Builders<MasterEnum>.IndexKeys.Ascending(u => u.Id)
                .Ascending(u => u.StandardizedEnum), new CreateIndexOptions { Unique = true, Name = "id_standardizedenum" });

#if DOTNET2
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<IMasterEnum, MasterEnum>(); });

            config.CreateMapper().Map<MasterEnum, IMasterEnum>(new MasterEnum());
#else
            Mapper.CreateMap<MasterEnum, IMasterEnum>().As<MasterEnum>();
#endif
        }

        private IMongoCollection<MasterEnum> Collection { get; }

        private ITenantService TenantService { get; }

        /// <summary>
        /// GetTenantFields
        /// </summary>
        /// <returns>list of TenantField object</returns>
        public async Task<ICollection<IMasterEnum>> GetMasterEnums(string enumname = "")
        {
            var builder = new FilterDefinitionBuilder<MasterEnum>();
            var filter = FilterDefinition<MasterEnum>.Empty;
            if (!string.IsNullOrWhiteSpace(enumname))
            {
                filter = builder.Where(x => x.StandardizedEnum.ToLower() == enumname.ToLower());
            }
            var found = await Collection.FindAsync(filter);
            var menums = await found.ToListAsync();

#if DOTNET2
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<List<IMasterEnum>, List<MasterEnum>>(); });

           return config.CreateMapper().Map<List<MasterEnum>, List<IMasterEnum>>(menums);
#else
            return Mapper.Map<List<MasterEnum>, List<IMasterEnum>>(menums);
#endif
        }

        public async Task<ICollection<IMasterEnum>> GetMasterEnumsByFieldName(string fieldname)
        {
            var builder = new FilterDefinitionBuilder<MasterEnum>();
            var filter = builder.Ne(x => x.StandardizedField, "ignore");
            if (!string.IsNullOrWhiteSpace(fieldname))
            {
                filter = builder.Eq(x => x.StandardizedField, fieldname);
            }
            var found = await Collection.FindAsync(filter);
            var mEnums = await found.ToListAsync();

#if DOTNET2
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<List<IMasterEnum>, List<MasterEnum>>(); });

            return Mapper.Map<List<MasterEnum>, List<IMasterEnum>>(mEnums);
#else
            return Mapper.Map<List<MasterEnum>, List<IMasterEnum>>(mEnums);
#endif
        }

        public async Task<ICollection<IMasterEnum>> GetMasterEnumsByUpdateAfter(DateTime updateAfterDate)
        {
            var builder = new FilterDefinitionBuilder<MasterEnum>();
            var filter = builder.Ne(x => x.StandardizedField, "ignore");
            if (updateAfterDate != DateTime.MinValue)
            {
                filter = builder.Gte(x => x.LastUpdatedDate, updateAfterDate);
            }
            var found = await Collection.FindAsync(filter);
            var menums = await found.ToListAsync();

#if DOTNET2
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<List<IMasterEnum>, List<MasterEnum>>(); });

           return config.CreateMapper().Map<List<MasterEnum>, List<IMasterEnum>>(menums);
#else
            return Mapper.Map<List<MasterEnum>, List<IMasterEnum>>(menums);
#endif
        }

        public async Task<IMasterEnum> Add(IMasterEnum menum)
        {
            if (menum == null)
                throw new ArgumentNullException(nameof(menum));
            menum.Id = ObjectId.GenerateNewId().ToString();

            var entry = new MasterEnum
            {
                Id = menum.Id,
                StandardizedEnum = menum.StandardizedEnum,
                StandardizedField = menum.StandardizedField,
                Label = menum.Label,
                SequenceNo = menum.SequenceNo,
                LastUpdatedDate = menum.LastUpdatedDate,
                LastUpdatedBy = menum.LastUpdatedBy
            };

            await Collection.InsertOneAsync(entry);
            return entry;
        }

        public async Task<IMasterEnum> Update(string enumname, IMasterEnum menum)
        {
            if (menum == null)
                throw new ArgumentNullException(nameof(menum));

            if (string.IsNullOrWhiteSpace(enumname))
                throw new ArgumentNullException(nameof(enumname));

            var enumModel = await GetMasterEnums(enumname);

            if (!enumModel.Any())
            {
                return await Add(menum);
                //throw new NotFoundException($"Enum {enumname} cannot be found");
            }
            else
            {
                var definition = new UpdateDefinitionBuilder<MasterEnum>()
                    .Set(u => u.Label, menum.Label)
                    .Set(u => u.LastUpdatedBy, menum.LastUpdatedBy)
                    .Set(u => u.LastUpdatedDate, DateTime.UtcNow);

                await Collection.UpdateOneAsync(FindByEnumname(enumname), definition);

                enumModel = await GetMasterEnums(enumname);
                return enumModel.ToList()[0];
            }
        }

        public async Task<bool> Delete(string enumname)
        {
            if (string.IsNullOrWhiteSpace(enumname))
                throw new ArgumentNullException(nameof(enumname));

            var result = await Collection.DeleteOneAsync(FindByEnumname(enumname));
            if (result.DeletedCount > 0)
            {
                return true;
            }
            else
            {
                throw new NotFoundException("Enum " + enumname + " Not Found");
            }
        }

        private Expression<Func<MasterEnum, bool>> FindByEnumname(string enumname)
        {
            return enm => enm.StandardizedEnum == enumname;
        }
    }
}