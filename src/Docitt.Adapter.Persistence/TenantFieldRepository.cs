﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Docitt.Adapter.Persistence
{
    public class TenantFieldRepository :
        MongoRepository<ITenantField, TenantField>, ITenantFieldRepository
    {
        static TenantFieldRepository()
        {
            BsonClassMap.RegisterClassMap<TenantField>(map =>
            {
                map.AutoMap();

                var type = typeof(TenantField);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public TenantFieldRepository(
            ITenantService tenantService,
            IMongoConfiguration configuration) : base(tenantService, configuration, "tenantfield")
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (tenantService == null)
                throw new ArgumentNullException(nameof(tenantService));

            if (string.IsNullOrWhiteSpace(configuration.ConnectionString))
                throw new ArgumentException("Connection String cannot be empty");

            if (string.IsNullOrWhiteSpace(configuration.Database))
                throw new ArgumentException("Database name cannot be empty");

            CreateIndexIfNotExists("tenant_id", Builders<ITenantField>.IndexKeys
                .Ascending(account => account.TenantId), false);

            CreateIndexIfNotExists("standardized_field", Builders<ITenantField>.IndexKeys
                .Ascending(account => account.TenantId)
                .Ascending(account => account.StandardizedField), true); // StandardizedField should be unique.

            CreateIndexIfNotExists("tenant_label", Builders<ITenantField>.IndexKeys
                .Ascending(account => account.TenantId)
                .Ascending(account => account.TenantLabel), false);

            CreateIndexIfNotExists("standardizedfield_label", Builders<ITenantField>.IndexKeys
                .Ascending(account => account.StandardizedField)
                .Ascending(account => account.TenantLabel), false);
        }

        /// <summary>
        /// GetTenantFields
        /// </summary>
        /// <returns>list of TenantField object</returns>
        public async Task<ICollection<ITenantField>> GetTenantFields(string fieldname = "")
        {
            if (!string.IsNullOrWhiteSpace(fieldname))

                {
                    return await Query.Where(x => x.StandardizedField.ToLower() == fieldname.ToLower()).ToListAsync();
                }
                return await Query.ToListAsync();

        }

        public async Task<ICollection<ITenantField>> GetTenantFieldsByUpdateAfter(DateTime updateAfterDate)
        {
            if (updateAfterDate != DateTime.MinValue)
            {
                return await Query.Where(x => x.LastUpdatedDate >= updateAfterDate).ToListAsync();
            }
            return await Query.ToListAsync();
        }

        public async Task<ITenantField> AddTenantField(ITenantField tfield)
        {
            if (tfield == null)
                throw new ArgumentNullException(nameof(tfield));

            var entry = new TenantField
            {
                StandardizedField = tfield.StandardizedField,
                TenantLabel = tfield.TenantLabel,
                TenantFieldType = tfield.TenantFieldType,
                LastUpdatedDate = tfield.LastUpdatedDate,
                LastUpdatedBy = tfield.LastUpdatedBy,
                TenantId = TenantService.Current.Id
            };

            await Collection.InsertOneAsync(entry);
            return entry;
        }

        public async Task<ITenantField> Update(string fieldname, ITenantField tfield)
        {
            if (tfield == null)
                throw new ArgumentNullException(nameof(tfield));

            if (string.IsNullOrWhiteSpace(fieldname))
                throw new ArgumentNullException(nameof(fieldname));

            var fieldModel = await GetTenantFields(fieldname);

            if (!fieldModel.Any())
            {
                return await AddTenantField(tfield);
                //throw new NotFoundException($"Field {fieldname} cannot be found");
            }
            else
            {
                var definition = new UpdateDefinitionBuilder<ITenantField>()
                    .Set(u => u.TenantLabel, tfield.TenantLabel)
                    .Set(u => u.LastUpdatedBy, tfield.LastUpdatedBy)
                    .Set(u => u.LastUpdatedDate, DateTime.UtcNow);

                await Collection.UpdateOneAsync(FindByFieldname(fieldname), definition);

                fieldModel = await GetTenantFields(fieldname);
                return fieldModel.ToList()[0];
            }
        }

        public async Task<bool> Delete(string fieldname)
        {
            if (string.IsNullOrWhiteSpace(fieldname))
                throw new ArgumentNullException(nameof(fieldname));

            var result = await Collection.DeleteOneAsync(FindByFieldname(fieldname));
            if (result.DeletedCount > 0)
            {
                return true;
            }
            else
            {
                throw new NotFoundException("Field " + fieldname + " Not Found");
            }
        }

        private Expression<Func<ITenantField, bool>> FindByFieldname(string fieldname)
        {
            return fld => fld.TenantId == TenantService.Current.Id && fld.StandardizedField == fieldname;
        }
    }
}