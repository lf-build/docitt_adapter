﻿using AutoMapper;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Docitt.Adapter.Persistence
{
    public class MasterFieldRepository : IMasterFieldRepository
    {
        static MasterFieldRepository()
        {
            BsonClassMap.RegisterClassMap<MasterField>(map =>
            {
                map.AutoMap();
            });
        }

        public MasterFieldRepository(IMongoConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (string.IsNullOrWhiteSpace(configuration.ConnectionString))
                throw new ArgumentException("Connection String cannot be empty");

            if (string.IsNullOrWhiteSpace(configuration.Database))
                throw new ArgumentException("Database name cannot be empty");

            var client = new MongoClient(configuration.ConnectionString);
            var database = client.GetDatabase(configuration.Database);
            Collection = database.GetCollection<MasterField>("masterfield");

            Collection.Indexes.CreateOneAsync(
                Builders<MasterField>.IndexKeys.Ascending(u => u.Id),
                new CreateIndexOptions { Unique = true, Name = "id" });

            Collection.Indexes.CreateOneAsync(
                Builders<MasterField>.IndexKeys.Ascending(u => u.Id)
                .Ascending(u => u.StandardizedField), new CreateIndexOptions { Unique = true, Name = "id_standardizedfield" });

#if DOTNET2
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<IMasterField, MasterField>(); });

            config.CreateMapper().Map<MasterField, IMasterField>(new MasterField());
#else
            Mapper.CreateMap<MasterField, IMasterField>().As<MasterField>();
#endif
        }

        private IMongoCollection<MasterField> Collection { get; }

        /// <summary>
        /// GetTenantFields
        /// </summary>
        /// <returns>list of TenantField object</returns>
        public async Task<ICollection<IMasterField>> GetMasterFields(string fieldname = "")
        {
            var builder = new FilterDefinitionBuilder<MasterField>();
            var filter = FilterDefinition<MasterField>.Empty;
            if (!string.IsNullOrWhiteSpace(fieldname))
            {
                filter = builder.Where(x => x.StandardizedField.ToLower() == fieldname.ToLower());
            }
            var found = await Collection.FindAsync(filter);
            var mfields = await found.ToListAsync();
#if DOTNET2
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<List<IMasterField>, List<MasterField>>(); });

            return config.CreateMapper().Map<List<MasterField>, List<IMasterField>>(mfields);
#else
            return Mapper.Map<List<MasterField>, List<IMasterField>>(mfields);
#endif
        }

        public async Task<ICollection<IMasterField>> GetMasterFieldsByUpdateAfter(DateTime updateAfterDate)
        {
            var builder = new FilterDefinitionBuilder<MasterField>();
            var filter = builder.Ne(x => x.StandardizedField, "ignore");
            if (updateAfterDate != DateTime.MinValue)
            {
                filter = builder.Gte(x => x.LastUpdatedDate, updateAfterDate);
            }
            var found = await Collection.FindAsync(filter);
            var mfields = await found.ToListAsync();
#if DOTNET2
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<List<IMasterField>, List<MasterField>>(); });

            return config.CreateMapper().Map<List<MasterField>, List<IMasterField>>(mfields);
#else
            return Mapper.Map<List<MasterField>, List<IMasterField>>(mfields);
#endif
        }

        public async Task<IMasterField> Add(IMasterField mfield)
        {
            if (mfield == null)
                throw new ArgumentNullException(nameof(mfield));
            mfield.Id = ObjectId.GenerateNewId().ToString();

            var entry = new MasterField
            {
                Id = mfield.Id,
                StandardizedField = mfield.StandardizedField,
                Label = mfield.Label,
                Type = mfield.Type,
                LastUpdatedDate = mfield.LastUpdatedDate,
                LastUpdatedBy = mfield.LastUpdatedBy
            };

            await Collection.InsertOneAsync(entry);
            return entry;
        }

        public async Task<IMasterField> Update(string fieldname, IMasterField mfield)
        {
            if (mfield == null)
                throw new ArgumentNullException(nameof(mfield));

            if (string.IsNullOrWhiteSpace(fieldname))
                throw new ArgumentNullException(nameof(fieldname));

            var fieldModel = await GetMasterFields(fieldname);

            if (!fieldModel.Any())
            {
                return await Add(mfield);
                //throw new NotFoundException($"Field {fieldname} cannot be found");
            }
            else
            {
                var definition = new UpdateDefinitionBuilder<MasterField>()
                    .Set(u => u.Label, mfield.Label)
                    .Set(u => u.LastUpdatedBy, mfield.LastUpdatedBy)
                    .Set(u => u.LastUpdatedDate, DateTime.UtcNow);

                await Collection.UpdateOneAsync(FindByFieldname(fieldname), definition);

                fieldModel = await GetMasterFields(fieldname);
                return fieldModel.ToList()[0];
            }
        }

        public async Task<bool> Delete(string fieldname)
        {
            if (string.IsNullOrWhiteSpace(fieldname))
                throw new ArgumentNullException(nameof(fieldname));

            var result = await Collection.DeleteOneAsync(FindByFieldname(fieldname));
            if (result.DeletedCount > 0)
            {
                return true;
            }
            else
            {
                throw new NotFoundException("Field " + fieldname + " Not Found");
            }
        }

        private Expression<Func<MasterField, bool>> FindByFieldname(string fieldname)
        {
            return fld => fld.StandardizedField == fieldname;
        }
    }
}