﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Docitt.Adapter.Persistence
{
    public class TenantEnumRepository :
        MongoRepository<ITenantEnum, TenantEnum>, ITenantEnumRepository
    {
        static TenantEnumRepository()
        {
            BsonClassMap.RegisterClassMap<TenantEnum>(map =>
            {
                map.AutoMap();

                var type = typeof(TenantEnum);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public TenantEnumRepository(
            ITenantService tenantService,
            IMongoConfiguration configuration) : base(tenantService, configuration, "tenantenum")
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (tenantService == null)
                throw new ArgumentNullException(nameof(tenantService));

            if (string.IsNullOrWhiteSpace(configuration.ConnectionString))
                throw new ArgumentException("Connection String cannot be empty");

            if (string.IsNullOrWhiteSpace(configuration.Database))
                throw new ArgumentException("Database name cannot be empty");

            CreateIndexIfNotExists("tenant_id", Builders<ITenantEnum>.IndexKeys
                .Ascending(account => account.TenantId), false);

            CreateIndexIfNotExists("standardized_field", Builders<ITenantEnum>.IndexKeys
                .Ascending(account => account.TenantId)
                .Ascending(account => account.StandardizedField), true);

            CreateIndexIfNotExists("standardized_enum", Builders<ITenantEnum>.IndexKeys
                .Ascending(account => account.TenantId)
                .Ascending(account => account.StandardizedField)
                .Ascending(account => account.StandardizedEnum), false);
        }

        /// <summary>
        /// GetTenantenums
        /// </summary>
        /// <returns>list of Tenantenum object</returns>
        public async Task<ICollection<ITenantEnum>> GetTenantEnums(string enumname = "")
        {
            if (!string.IsNullOrWhiteSpace(enumname))
            {
                return await Query.Where(x => x.StandardizedEnum.ToLower() == enumname.ToLower()).ToListAsync();
            }

            return await  Query.ToListAsync();
        }

        public async Task<ICollection<ITenantEnum>> GetTenantEnumsByFieldName(string fieldname)
        {
            if (!string.IsNullOrWhiteSpace(fieldname))
            {
                return await Query.Where(x => x.StandardizedField == fieldname).ToListAsync();
            }
            return await Query.ToListAsync();
        }

        public async Task<ICollection<ITenantEnum>> GetTenantEnumsByUpdateAfter(DateTime updateAfterDate)
        {
            if (updateAfterDate != DateTime.MinValue)
            {
                return await Query.Where(x => x.LastUpdatedDate >= updateAfterDate).ToListAsync();
            }
            return await Query.ToListAsync();
        }

        public async Task<ITenantEnum> AddTenantEnum(ITenantEnum tenum)
        {
            if (tenum == null)
                throw new ArgumentNullException(nameof(tenum));

            var entry = new TenantEnum
            {
                StandardizedEnum = tenum.StandardizedEnum,
                StandardizedField = tenum.StandardizedField,
                TenantLabel = tenum.TenantLabel,
                SequenceNo = tenum.SequenceNo,
                LastUpdatedDate = tenum.LastUpdatedDate,
                LastUpdatedBy = tenum.LastUpdatedBy,
                Visible = tenum.Visible,
                TenantId = TenantService.Current.Id
            };

            await Collection.InsertOneAsync(entry);
            return entry;
        }

        public async Task<ITenantEnum> Update(string enumname, ITenantEnum tenum)
        {
            if (tenum == null)
                throw new ArgumentNullException(nameof(tenum));

            if (string.IsNullOrWhiteSpace(enumname))
                throw new ArgumentNullException(nameof(enumname));

            var enumModel = await GetTenantEnums(enumname);

            if (!enumModel.Any())
            {
                return await AddTenantEnum(tenum);
                //throw new NotFoundException($"Enum {enumname} cannot be found");
            }
            else
            {
                var definition = new UpdateDefinitionBuilder<ITenantEnum>()
                .Set(u => u.TenantLabel, tenum.TenantLabel)
                .Set(u => u.LastUpdatedBy, tenum.LastUpdatedBy)
                .Set(u => u.LastUpdatedDate, DateTime.UtcNow);

                await Collection.UpdateOneAsync(FindByEnumname(enumname), definition);

                enumModel = await GetTenantEnums(enumname);
                return enumModel.ToList()[0];
            }
        }

        public async Task<bool> Delete(string enumname)
        {
            if (string.IsNullOrWhiteSpace(enumname))
                throw new ArgumentNullException(nameof(enumname));

            var result = await Collection.DeleteOneAsync(FindByEnumname(enumname));
            if (result.DeletedCount > 0)
            {
                return true;
            }
            else
            {
                throw new NotFoundException("Enum " + enumname + " Not Found");
            }
        }

        private Expression<Func<ITenantEnum, bool>> FindByEnumname(string enumname)
        {
            return enm => enm.TenantId == TenantService.Current.Id && enm.StandardizedEnum == enumname;
        }
    }
}