﻿using System;

namespace Docitt.Adapter
{
    public static class Settings
    {        
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "adapter";
    }
}