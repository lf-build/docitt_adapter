﻿using System.Threading.Tasks;

namespace Docitt.Adapter
{
    public interface IAdapterHelperService
    {
        Task<ITransformDataResponse> GetTransformRuleResult(dynamic originalData, string applicationId, string borrowerId = null);
    }
}