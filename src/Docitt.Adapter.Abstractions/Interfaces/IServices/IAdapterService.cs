﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Adapter
{
    public interface IAdapterService
    {

        Task<List<IMasterField>> GetMasterField(string fieldname = "");
        Task<List<IMasterEnum>> GetMasterEnum(string enumname = "");
        Task<List<IMasterEnum>> GetMasterEnumsByFieldName(string fieldname);
        Task<List<ITenantField>> GetTenantField(string fieldname = "");
        Task<List<ITenantEnum>> GetTenantEnum(string enumname = "");
        Task<List<ITenantEnum>> GetTenantEnumsByFieldName(string fieldname);

        Task<IMasterField> AddMasterField(IMasterField field);

        Task<IMasterEnum> AddMasterEnum(IMasterEnum data);

        Task<ITenantField> AddTenantField(ITenantField field);

        Task<ITenantEnum> AddTenantEnum(ITenantEnum data);

        Task<ITenantFieldEnumCustom> GetEnumFieldDetail();

        Task<List<ITenantFieldCustom>> GetTenantFieldDetail(string fieldName);

        Task<List<IMasterField>> GetMasterFieldsByUpdateAfter(DateTime updateAfterDate);

        Task<List<IMasterEnum>> GetMasterEnumsByUpdateAfter(DateTime updateAfterDate);

        Task<List<ITenantField>> GetTenantFieldsByUpdateAfter(DateTime updateAfterDate);

        Task<List<ITenantEnum>> GetTenantEnumsByUpdateAfter(DateTime updateAfterDate);

        Task<IMasterField> UpdateMasterField(string fieldname, IMasterField field);

        Task<IMasterEnum> UpdateMasterEnum(string enumname, IMasterEnum data);

        Task<ITenantField> UpdateTenantField(string fieldname, ITenantField field);

        Task<ITenantEnum> UpdateTenantEnum(string enumname, ITenantEnum data);

        Task<bool> DeleteMasterField(string fieldname);

        Task<bool> DeleteMasterEnum(string enumname);

        Task<bool> DeleteTenantField(string fieldname);

        Task<bool> DeleteTenantEnum(string enumname);
    }
}