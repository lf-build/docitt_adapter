﻿using System.Threading.Tasks;

namespace Docitt.Adapter
{
    public interface IBorrowerAdapterService
    {
        /// <summary>
        /// Get the Profile section data
        /// </summary>
        /// <param name="applicationId">application number</param>
        /// <param name="borrowerId">applicant username</param>
        /// <returns></returns>
        Task<ITransformDataResponse> GetProfileTransformData(string applicationId, string borrowerId);
        
        /// <summary>
        /// Get the Asset section data
        /// </summary>
        /// <param name="applicationId">application number</param>
        /// <param name="borrowerId">applicant username</param>
        /// <returns></returns>
        Task<ITransformDataResponse> GetAssetTransformData(string applicationId, string borrowerId);

        /// <summary>
        /// Get the Income section data
        /// </summary>
        /// <param name="applicationId">application number</param>
        /// <param name="borrowerId">applicant username</param>
        /// <returns></returns>
        Task<ITransformDataResponse> GetIncomeTransformData(string applicationId, string borrowerId);

        /// <summary>
        /// Get the Declaration section data
        /// </summary>
        /// <param name="applicationId">application number</param>
        /// <param name="borrowerId">applicant username</param>
        /// <returns></returns>
        Task<ITransformDataResponse> GetDeclarationTransformData(string applicationId, string borrowerId);

        /// <summary>
        /// Get the all section data
        /// </summary>
        /// <param name="applicationId">application number</param>
        /// <param name="borrowerId">applicant username</param>
        /// <returns></returns>
        Task<ITransformDataResponse> GetAllSectionTransformData(string applicationId, string borrowerId);
    }
}