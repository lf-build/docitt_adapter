﻿using System.Threading.Tasks;

namespace Docitt.Adapter
{
    public interface ITransformApplicationService
    {
        Task<bool> CheckIsApplicationSubmitted(string applicationId);
        
        Task<ITransformDataResponse> GetLoanTransformData(string applicationId);

        Task<ITransformDataResponse> GetConditionTransformData(string applicationId);

        Task<ITransformDataResponse> GetConditionDocumentsTransformData(string applicationId, string conditionId);

        Task<ITransformDataResponse> GetApplicationDocumentsTransformData(string applicationId);

        Task<ITransformDataResponse> GetApplicationTransformData(string applicationId);
    }
}