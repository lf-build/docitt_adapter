﻿namespace Docitt.Adapter
{
    public interface ILoanOriginalApplicantDataResponse
    {
        string BorrowerId { get; set; }
        string BorrowerEmail { get; set; }
        string Role { get; set; }
    }
}