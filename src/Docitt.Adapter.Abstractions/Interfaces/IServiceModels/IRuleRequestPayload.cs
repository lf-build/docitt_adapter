﻿namespace Docitt.Adapter
{
    public interface IRuleRequestPayload
    {
        dynamic OriginalData { get; set; }

        ITenantFieldEnumCustom MasterData { get; set; }

        string TenantId { get; set; }

        string ApplicationId { get; set; }

        string BorrowerId { get; set; }
    }
}