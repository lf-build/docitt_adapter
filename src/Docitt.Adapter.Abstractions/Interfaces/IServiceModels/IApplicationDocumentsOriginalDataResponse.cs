﻿using System.Collections.Generic;

namespace Docitt.Adapter
{
    public interface IApplicationDocumentsOriginalDataResponse
    {
        string Id { get; set; }
        string FileName { get; set; }
        long Size { get; set; }
        List<string> Tags { get; set; }
        object Metadata { get; set; }
        string Timestamp { get; set; }
    }
}