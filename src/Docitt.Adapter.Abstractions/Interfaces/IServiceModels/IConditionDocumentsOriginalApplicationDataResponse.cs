﻿using System.Collections.Generic;

namespace Docitt.Adapter
{
    public interface IConditionDocumentsOriginalApplicationDataResponse
    {
        string ConditionId { get; set; }
        string ConditionFileName { get; set; }
        long ConditionFileSize { get; set; }
        List<string> ConditionFileTags { get; set; }
    }
}