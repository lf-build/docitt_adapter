﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Adapter
{
    public interface ILoanOriginalApplicationDataResponse
    {
        string ApplicationNumber { get; set; }
        string ApplicationPurpose { get; set; }
        double LoanAmount { get; set; }
        string ApplicationCreatedDate { get; set; }
        string ApplicationStatus { get; set; }
        string ApplicationStatusCode { get; set; }
        string ApplicationCompletedDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ILoanOriginalApplicantDataResponse, LoanOriginalApplicantDataResponse>))]
        List<ILoanOriginalApplicantDataResponse> Applicants { get; set; }

        IPropertyAddress PropertyAddress { get; set; }
        double PropertyValue { get; set; }
    }
}