﻿using System.Collections.Generic;

namespace Docitt.Adapter
{
    public interface IApplicationDetailedOriginalDataResponse
    {
        ILoanOriginalApplicationDataResponse LoanApplication { get; set; }
        List<IConditionOriginalApplicationDataResponse> Conditions { get; set; }
    }
}