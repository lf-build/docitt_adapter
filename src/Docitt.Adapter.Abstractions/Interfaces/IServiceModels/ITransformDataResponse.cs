﻿namespace Docitt.Adapter
{
    public interface ITransformDataResponse
    {
        string TenantId { get; set; }

        string ApplicationId { get; set; }

        string BorrowerId { get; set; }

        object OriginalData { get; set; }

        object[] TransformedData { get; set; }
        string Result { get; set; }
    }
}