﻿namespace Docitt.Adapter
{
    public interface IConditionOriginalApplicationDataResponse
    {
        string ConditionId { get; set; }
        string ApplicationNumber { get; set; }
        string ConditionRequestedDate { get; set; }
        string ConditionDueDate { get; set; }
        string ConditionCompletedDate { get; set; }
        string ConditionPriority { get; set; }
        string ConditionCategory { get; set; }
        string ConditionSubCategory { get; set; }
        string ConditionTitle { get; set; }
        string ConditionShortName { get; set; }
        string ConditionStatus { get; set; }
    }
}