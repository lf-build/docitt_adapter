﻿namespace Docitt.Adapter
{
    public interface IPropertyAddress
    {
        string Address { get; set; }

        string Address2 { get; set; }

        string AddressCity { get; set; }

        string AddressState { get; set; }

        string AddressZip { get; set; }

        string AddressCounty {get; set;}
    }
}