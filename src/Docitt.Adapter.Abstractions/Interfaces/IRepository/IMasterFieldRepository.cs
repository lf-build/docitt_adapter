﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Adapter
{
    public interface IMasterFieldRepository
    {
        Task<IMasterField> Add(IMasterField mfield);

        Task<ICollection<IMasterField>> GetMasterFields(string fieldname);

        Task<ICollection<IMasterField>> GetMasterFieldsByUpdateAfter(DateTime updateAfterDate);

        Task<IMasterField> Update(string fieldname, IMasterField mfield);

        Task<bool> Delete(string fieldname);
    }
}