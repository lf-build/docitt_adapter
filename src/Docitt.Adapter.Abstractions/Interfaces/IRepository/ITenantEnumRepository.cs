﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Adapter
{
    public interface ITenantEnumRepository : IRepository<ITenantEnum>
    {
        Task<ICollection<ITenantEnum>> GetTenantEnums(string enumname);

        Task<ICollection<ITenantEnum>> GetTenantEnumsByFieldName(string fieldname);

        Task<ICollection<ITenantEnum>> GetTenantEnumsByUpdateAfter(DateTime updateAfterDate);

        Task<ITenantEnum> AddTenantEnum(ITenantEnum tenum);

        Task<ITenantEnum> Update(string enumname, ITenantEnum tenum);

        Task<bool> Delete(string enumname);
    }
}