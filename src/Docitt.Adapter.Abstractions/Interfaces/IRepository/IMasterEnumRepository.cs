﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Adapter
{
    public interface IMasterEnumRepository
    {
        Task<ICollection<IMasterEnum>> GetMasterEnums(string enumname);

        Task<ICollection<IMasterEnum>> GetMasterEnumsByFieldName(string fieldname);

        Task<ICollection<IMasterEnum>> GetMasterEnumsByUpdateAfter(DateTime updateAfterDate);

        Task<IMasterEnum> Add(IMasterEnum menum);

        Task<IMasterEnum> Update(string enumname, IMasterEnum menum);

        Task<bool> Delete(string enumname);
    }
}