﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Adapter
{
    public interface ITenantFieldRepository : IRepository<ITenantField>
    {
        Task<ICollection<ITenantField>> GetTenantFields(string fieldname);

        Task<ICollection<ITenantField>> GetTenantFieldsByUpdateAfter(DateTime updateAfterDate);

        Task<ITenantField> AddTenantField(ITenantField tfield);

        Task<ITenantField> Update(string fieldname, ITenantField tfield);

        Task<bool> Delete(string fieldname);
    }
}