﻿namespace Docitt.Adapter
{
    public class RuleRequestPayload : IRuleRequestPayload
    {
        public RuleRequestPayload()
        {
        }

        public RuleRequestPayload(dynamic originalData, ITenantFieldEnumCustom masterData, string tenantId, string applicationId)
        {
            if (originalData != null && masterData != null)
            {
                OriginalData = originalData;
                MasterData = masterData;
                TenantId = tenantId;
                ApplicationId = applicationId;
            }
        }

        public dynamic OriginalData { get; set; }

        public ITenantFieldEnumCustom MasterData { get; set; }

        public string TenantId { get; set; }

        public string ApplicationId { get; set; }

        public string BorrowerId { get; set; }
    }
}