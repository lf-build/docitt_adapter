﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace Docitt.Adapter
{
    public interface IConfiguration: IDependencyConfiguration
    {
        string TransformRule { get; set; }
        string GetBankInfoRule { get; set; }

        

        string[] TreeViewTextBoxControl { get; set; }

        string[] RefinanceAddressSameList { get; set; }

        string[] SpouseInfoQuestionList { get; set; }
    }

    public class Configuration : IConfiguration
    {
        public string TransformRule { get; set; }

        public string GetBankInfoRule { get; set; }
        
        public Dictionary<string,string> Dependencies { get; set; }
        public string Database { get; set; }

        public string ConnectionString { get; set; }

         public string[] TreeViewTextBoxControl { get; set; }

         public string[] RefinanceAddressSameList { get; set; }

         public string[] SpouseInfoQuestionList { get; set; }
    }
}