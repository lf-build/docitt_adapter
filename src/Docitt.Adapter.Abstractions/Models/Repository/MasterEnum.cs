﻿using System;

namespace Docitt.Adapter
{
    public interface IMasterEnum
    {
        string Id { get; set; }
        string StandardizedEnum { get; set; }
        string StandardizedField { get; set; }
        string Label { get; set; }
        int SequenceNo { get; set; }
        DateTime LastUpdatedDate { get; set; }
        string LastUpdatedBy { get; set; }
    }

    public class MasterEnum : IMasterEnum
    {
        public string Id { get; set; }
        public string StandardizedEnum { get; set; }
        public string StandardizedField { get; set; }
        public string Label { get; set; }
        public int SequenceNo { get; set; }
        public DateTime LastUpdatedDate { get; set; } = DateTime.UtcNow;
        public string LastUpdatedBy { get; set; } = "System";
    }
}