﻿using LendFoundry.Foundation.Persistence;
using System;

namespace Docitt.Adapter
{
    public interface ITenantField : IAggregate
    {
        string StandardizedField { get; set; }
        string TenantLabel { get; set; }
        string TenantFieldType { get; set; }
        DateTime LastUpdatedDate { get; set; }
        string LastUpdatedBy { get; set; }
    }

    public class TenantField : Aggregate, ITenantField
    {
        public string StandardizedField { get; set; }
        public string TenantLabel { get; set; }
        public string TenantFieldType { get; set; }
        public DateTime LastUpdatedDate { get; set; } = DateTime.UtcNow;
        public string LastUpdatedBy { get; set; } = "System";
    }
}