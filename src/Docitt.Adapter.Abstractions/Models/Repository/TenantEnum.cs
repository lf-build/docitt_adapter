﻿using LendFoundry.Foundation.Persistence;
using System;

namespace Docitt.Adapter
{
    public interface ITenantEnum : IAggregate
    {
        string StandardizedEnum { get; set; }
        string StandardizedField { get; set; }
        string TenantLabel { get; set; }
        int SequenceNo { get; set; }
        bool Visible { get; set; }
        DateTime LastUpdatedDate { get; set; }
        string LastUpdatedBy { get; set; }
    }

    public class TenantEnum : Aggregate, ITenantEnum
    {
        public string StandardizedEnum { get; set; }
        public string StandardizedField { get; set; }
        public string TenantLabel { get; set; }
        public int SequenceNo { get; set; }
        public bool Visible { get; set; } = true;
        public DateTime LastUpdatedDate { get; set; } = DateTime.UtcNow;
        public string LastUpdatedBy { get; set; } = "System";
    }
}