﻿using System;

namespace Docitt.Adapter
{
    public interface IMasterField
    {
        string Id { get; set; }
        string StandardizedField { get; set; }
        string Label { get; set; }
        string Type { get; set; }
        DateTime LastUpdatedDate { get; set; }
        string LastUpdatedBy { get; set; }
    }

    public class MasterField : IMasterField
    {
        public string Id { get; set; }
        public string StandardizedField { get; set; }
        public string Label { get; set; }
        public string Type { get; set; }
        public DateTime LastUpdatedDate { get; set; } = DateTime.UtcNow;
        public string LastUpdatedBy { get; set; } = "System";
    }
}