﻿namespace Docitt.Adapter
{
    public class TransformDataResponse : ITransformDataResponse
    {
        public string TenantId { get; set; }

        public string ApplicationId { get; set; }

        public string BorrowerId { get; set; }

        public object OriginalData { get; set; }

        public object[] TransformedData { get; set; }

        public string Result { get; set; }
    }
}