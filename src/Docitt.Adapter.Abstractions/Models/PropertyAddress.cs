﻿using Docitt.Application;

namespace Docitt.Adapter
{
    public class PropertyAddress : IPropertyAddress
    {
        public PropertyAddress()
        {
        }

        public PropertyAddress(IAddress propertyAddress)
        {
            Address = propertyAddress.Line1;
            Address2 = propertyAddress.Line2;
            AddressCity = propertyAddress.City;
            AddressState = propertyAddress.State;
            AddressZip = propertyAddress.ZipCode;
            AddressCounty = propertyAddress.County;
        }

        public string Address { get; set; }
        public string Address2 { get; set; }

        public string AddressCity { get; set; }

        public string AddressState { get; set; }

        public string AddressZip { get; set; }

        public string AddressCounty {get; set;}
    }
}