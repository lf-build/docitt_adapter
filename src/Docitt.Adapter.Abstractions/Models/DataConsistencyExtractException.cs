using System;

namespace Docitt.Adapter
{
    public class DataConsistencyExtractException : Exception
    {
        public DataConsistencyExtractException(string message):base(message)
        {}
    }
}