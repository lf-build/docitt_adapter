﻿using LendFoundry.DocumentManager;
using System.Collections.Generic;

namespace Docitt.Adapter
{
    public class ApplicationDocumentsOriginalDataResponse : IApplicationDocumentsOriginalDataResponse
    {
        public ApplicationDocumentsOriginalDataResponse(IDocument response)
        {
            if (response != null)
            {
                Id = response.Id;
                FileName = response.FileName;
                Size = response.Size;
                Tags = response.Tags;
                Metadata = response.Metadata;
                Timestamp = response.Timestamp.ToString("yyyy-MM-dd HH:mm:ss");
            }
        }

        public string FileName { get; set; }

        public string Id { get; set; }

        public long Size { get; set; }

        public List<string> Tags { get; set; }
        public object Metadata { get; set; }
        public string Timestamp { get; set; }
    }
}