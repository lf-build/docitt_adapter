﻿using LendFoundry.DocumentManager;
using System.Collections.Generic;

namespace Docitt.Adapter
{
    public class ConditionDocumentsOriginalApplicationDataResponse : IConditionDocumentsOriginalApplicationDataResponse
    {
        public ConditionDocumentsOriginalApplicationDataResponse(string conditionId, IDocument response)
        {
            if (response != null)
            {
                ConditionId = conditionId;
                ConditionFileName = response.FileName;
                ConditionFileSize = response.Size;
                if (response.Tags != null)
                    ConditionFileTags = response.Tags;
            }
        }

        public string ConditionFileName { get; set; }

        public long ConditionFileSize { get; set; }

        public List<string> ConditionFileTags { get; set; }

        public string ConditionId { get; set; }
    }
}