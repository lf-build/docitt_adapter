﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace Docitt.Adapter
{
    public interface ITenantFieldCustom : IAggregate, ITenantField
    {
        string DefaultLabel { get; set; }
        List<ITenantEnumCustom> Enums { get; set; }
    }

    public class TenantFieldCustom : Aggregate, ITenantFieldCustom
    {
        public string StandardizedField { get; set; }
        public string DefaultLabel { get; set; }
        public string TenantLabel { get; set; }
        public string TenantFieldType { get; set; }
        public DateTime LastUpdatedDate { get; set; } = DateTime.UtcNow;
        public string LastUpdatedBy { get; set; } = "System";
        public List<ITenantEnumCustom> Enums { get; set; }
    }

    public interface ITenantEnumCustom : IAggregate, ITenantEnum
    {
        string DefaultLabel { get; set; }
    }

    public class TenantEnumCustom : Aggregate, ITenantEnumCustom
    {
        public string StandardizedEnum { get; set; }
        public string StandardizedField { get; set; }
        public string DefaultLabel { get; set; }
        public string TenantLabel { get; set; }
        public int SequenceNo { get; set; }
        public bool Visible { get; set; } = true;
        public DateTime LastUpdatedDate { get; set; } = DateTime.UtcNow;
        public string LastUpdatedBy { get; set; } = "System";
    }

    public interface ITenantFieldEnumCustom
    {
        List<ITenantField> TenantFields { get; set; }
        List<ITenantEnum> TenantEnums { get; set; }
    }

    public class TenantFieldEnumCustom : ITenantFieldEnumCustom
    {
        public List<ITenantField> TenantFields { get; set; }
        public List<ITenantEnum> TenantEnums { get; set; }
    }
}