﻿using Docitt.RequiredCondition;

namespace Docitt.Adapter
{
    public class ConditionOriginalApplicationDataResponse : IConditionOriginalApplicationDataResponse
    {
        public ConditionOriginalApplicationDataResponse(IRequest response)
        {
            if (response != null)
            {
                ConditionId = response.Id;
                ApplicationNumber = response.EntityId;
                if (response.RequestedDate != null)
                    ConditionRequestedDate = response.RequestedDate.Time.ToString("yyyy-MM-dd HH:mm:ss");
                if (response.DueDate != null)
                    ConditionDueDate = response.DueDate.Time.ToString("yyyy-MM-dd HH:mm:ss");
                if (response.CompletedDate != null)
                    ConditionCompletedDate = response.CompletedDate.Time.ToString("yyyy-MM-dd HH:mm:ss");

                ConditionPriority = response.Priority.ToString();
                if (response.RequestType != null)
                {
                    var requestType = response.RequestType;
                    ConditionCategory = requestType.ConditionCategory;
                    ConditionSubCategory = requestType.ConditionSubCategory;
                    ConditionTitle = requestType.Title;
                    ConditionShortName = requestType.RequestId;
                }

                ConditionStatus = response.StatusLabel;
                Recipients = response.Recipients;
            }
        }

        public string ApplicationNumber { get; set; }

        public string ConditionCategory { get; set; }

        public string ConditionSubCategory { get; set; }
        public string ConditionCompletedDate { get; set; }

        public string ConditionDueDate { get; set; }

        public string ConditionId { get; set; }

        public string ConditionPriority { get; set; }

        public string ConditionRequestedDate { get; set; }

        public string ConditionShortName { get; set; }

        public string ConditionStatus { get; set; }

        public string ConditionTitle { get; set; }

        public string[]  Recipients {get; set;}
    }
}