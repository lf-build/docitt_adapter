﻿using System.Collections.Generic;

namespace Docitt.Adapter
{
    public class ApplicationDetailedOriginalDataResponse : IApplicationDetailedOriginalDataResponse
    {
        public List<IConditionOriginalApplicationDataResponse> Conditions { get; set; }

        public ILoanOriginalApplicationDataResponse LoanApplication { get; set; }
    }
}