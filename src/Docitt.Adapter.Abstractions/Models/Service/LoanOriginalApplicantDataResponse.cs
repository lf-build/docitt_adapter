﻿using System.Collections.Generic;
using System.Linq;
using Docitt.Application;
using Docitt.Questionnaire;

namespace Docitt.Adapter
{
    public class LoanOriginalApplicantDataResponse : ILoanOriginalApplicantDataResponse
    {
        public LoanOriginalApplicantDataResponse(IApplicantRequest response,List<IBorrowerCoBorrowerInfo> borrowerCoborrowerInfoList = null)
        {
            if (response != null)
            {
                IBorrowerCoBorrowerInfo  borrowerCoborrowerInfo = null;
                 if(borrowerCoborrowerInfoList != null)
                    {
                         borrowerCoborrowerInfo =  borrowerCoborrowerInfoList.FirstOrDefault(x=> x.Email.ToLower() == response.Email.ToLower());
                    }

                BorrowerId = response.ApplicantId;
                BorrowerEmail = response.Email;
                if(borrowerCoborrowerInfo!= null)
                {
                         Role = borrowerCoborrowerInfo.ApplicantType.ToString();
                }
                else{
                        Role = response.IsPrimary ? "Borrower" : "CoBorrower";
                }
            }
        }

        public string BorrowerEmail { get; set; }

        public string BorrowerId { get; set; }

        public string Role { get; set; }
    }
}