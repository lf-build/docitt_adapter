﻿using Docitt.Application;
using System.Collections.Generic;
using System.Linq;
using Docitt.Questionnaire;

namespace Docitt.Adapter
{
    public class LoanOriginalApplicationDataResponse : ILoanOriginalApplicationDataResponse
    {
        public LoanOriginalApplicationDataResponse(IApplicationResponse response,List<IBorrowerCoBorrowerInfo> borrowerCoborrowerInfoList = null)
        {
            if (response != null)
            {
                ApplicationNumber = response.ApplicationNumber;
                ApplicationPurpose = response.Purpose;
                LoanAmount = response.Amount;
                ApplicationCreatedDate = response.SubmittedDate.Time.ToString("yyyy-MM-dd HH:mm:ss");
                if (response.Status != null)
                {
                    ApplicationStatus = response.Status.Label;
                    ApplicationStatusCode = response.Status.Code;
                }
                ApplicationCompletedDate = response.ExpirationDate.Time.ToString("yyyy-MM-dd HH:mm:ss");

                if (response.Applicants != null)
                {                   

                    Applicants = response.Applicants.Select(p => new LoanOriginalApplicantDataResponse(p,borrowerCoborrowerInfoList)).ToList<ILoanOriginalApplicantDataResponse>();
                }
                if (response.PropertyAddress != null)
                {
                    PropertyAddress = new PropertyAddress(response.PropertyAddress);
                }
                PropertyValue = response.PropertyValue;
            }
        }

        public string ApplicationNumber { get; set; }

        public string ApplicationPurpose { get; set; }

        public string ApplicationStatus { get; set; }

        public string ApplicationStatusCode { get; set; }

        public List<ILoanOriginalApplicantDataResponse> Applicants { get; set; }

        public string ApplicationCompletedDate { get; set; }

        public string ApplicationCreatedDate { get; set; }

        public double LoanAmount { get; set; }

        public IPropertyAddress PropertyAddress { get; set; }

        public double PropertyValue { get; set; }
    }
}