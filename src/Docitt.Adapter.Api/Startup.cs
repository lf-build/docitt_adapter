﻿using Docitt.Adapter.Persistence;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using Docitt.RequiredCondition.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DocumentManager.Client;
using Docitt.Application.Client;
using Docitt.Questionnaire.Client;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using LendFoundry.Foundation.ServiceDependencyResolver;

namespace Docitt.Adapter.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "DocittAdapter"
                });
                     c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    { "Bearer", new string[] { } }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Docitt.Adapter.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);

            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddQuestionnaireService();
            services.AddDecisionEngine();

            services.AddApplicationService();
            services.AddRequiredConditionService();
            services.AddDocumentManager();
            services.AddTransient<IConfiguration>(provider => provider.GetRequiredService<IConfigurationService<Configuration>>().Get());

            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            //Repositories
            services.AddTransient<ITenantFieldRepository, TenantFieldRepository>();
            services.AddTransient<ITenantEnumRepository, TenantEnumRepository>();
            services.AddTransient<IMasterEnumRepository, MasterEnumRepository>();
            services.AddTransient<IMasterFieldRepository, MasterFieldRepository>();
            //Services
            services.AddTransient<ITransformApplicationService, TransformApplicationService>();
            services.AddTransient<IAdapterService, AdapterService>();
            services.AddTransient<IAdapterHelperService, AdapterHelperService>();
            services.AddTransient<IBorrowerAdapterService, BorrowerAdapterService>();

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		    app.UseCors(env);
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "DOCITT Adapter Service");
            });
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
             app.UseConfigurationCacheDependency();
        }
    }
}