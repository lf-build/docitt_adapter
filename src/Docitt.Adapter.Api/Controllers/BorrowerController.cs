﻿﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Docitt.Adapter.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Docitt.Adapter.Api.Controllers
{
    /// <summary>
    /// Represents borrower controller.
    /// </summary>
    public class BorrowerController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="adapterService"></param>
        public BorrowerController(ILogger logger, IBorrowerAdapterService adapterService):base(logger)
        {
            AdapterService = adapterService;
        }    

        /// <summary>
        /// Borrower adapter service
        /// </summary>
        private IBorrowerAdapterService AdapterService { get; }

        /// <summary>
        /// Get the transform data for questionnaire profile
        /// </summary>
        /// <param name="applicationId">The application id</param>
        /// <param name="borrowerId">The borrower id or user name</param>
        /// <returns>Transform data object</returns>
        [HttpGet("application/{applicationId}/borrower/{borrowerId}/profile")]
        [ProducesResponseType(typeof(ITransformDataResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetProfileSectionTransformData(string applicationId, string borrowerId)
        {
            Logger.Debug("Started Get Questionnaire Profile section data..");
            borrowerId = StringExtensions.ConvertToDocittCaseSensitiveType(borrowerId);
            try
            {
                var result = await AdapterService.GetProfileTransformData(applicationId, borrowerId);
                Logger.Debug("Ended Get Questionnaire Profile section data...");
                return Ok(result);
            }
            catch(DataConsistencyExtractException dc)
            {
                Logger.Error(dc.Message, dc);
                return ErrorResult.InternalServerError(dc.Message);
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.InternalServerError("Server error. Try later.");
            }
        }

        /// <summary>
        /// Get the transform data for questionnaire asset
        /// </summary>
        /// <param name="applicationId">The application id</param>
        /// <param name="borrowerId">The borrower id or user name</param>
        /// <returns>Transform data object</returns>
        [HttpGet("application/{applicationId}/borrower/{borrowerId}/asset")]
        [ProducesResponseType(typeof(ITransformDataResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAssetSectionTransformData(string applicationId, string borrowerId)
        {
            try
            {
                Logger.Debug("Started Get Questionnaire Asset section data..");
                borrowerId = StringExtensions.ConvertToDocittCaseSensitiveType(borrowerId);
                var result = await AdapterService.GetAssetTransformData(applicationId, borrowerId);
                Logger.Debug("Ended Get Questionnaire Asset section data...");
                return Ok(result);
            }
            catch(DataConsistencyExtractException dc)
            {
                Logger.Error(dc.Message, dc);
                return ErrorResult.InternalServerError(dc.Message);
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.InternalServerError("Server error. Try later.");
            }
            
        }

        /// <summary>
        /// Get the transform data for questionnaire income
        /// </summary>
        /// <param name="applicationId">The application id</param>
        /// <param name="borrowerId">The borrower id or user name</param>
        /// <returns>Transform data object</returns>
        [HttpGet("application/{applicationId}/borrower/{borrowerId}/income")]
        [ProducesResponseType(typeof(ITransformDataResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetIncomeSectionTransformData(string applicationId, string borrowerId)
        {
            Logger.Debug("Started Get Questionnaire Income section data..");
            try
            {
                borrowerId = StringExtensions.ConvertToDocittCaseSensitiveType(borrowerId);
                var result = await AdapterService.GetIncomeTransformData(applicationId, borrowerId);
                Logger.Debug("Ended Get Questionnaire Income section data...");
                return Ok(result);
            }
            catch(DataConsistencyExtractException dc)
            {
                Logger.Error(dc.Message, dc);
                return ErrorResult.InternalServerError(dc.Message);
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.InternalServerError("Server error. Try later.");
            }
        }

        /// <summary>
        /// Get the transform data for questionnaire declaration
        /// </summary>
        /// <param name="applicationId">The application id</param>
        /// <param name="borrowerId">The borrower id or user name</param>
        /// <returns>Transform data object</returns>
        [HttpGet("application/{applicationId}/borrower/{borrowerId}/declaration")]
        [ProducesResponseType(typeof(ITransformDataResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetDeclarationSectionTransformData(string applicationId, string borrowerId)
        {
            Logger.Debug("Started Get Questionnaire declaration section data..");
            try
            {
                borrowerId = StringExtensions.ConvertToDocittCaseSensitiveType(borrowerId);
                var result = await AdapterService.GetDeclarationTransformData(applicationId, borrowerId);
                Logger.Debug("Ended Get Questionnaire declaration section data...");
                return Ok(result);
            }
            catch(DataConsistencyExtractException dc)
            {
                Logger.Error(dc.Message, dc);
                return ErrorResult.InternalServerError(dc.Message);
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.InternalServerError("Server error. Try later.");
            }
        }

        /// <summary>
        /// Get the transform data for questionnaire all section (profile,asset,income,declaration)
        /// </summary>
        /// <param name="applicationId">The application id</param>
        /// <param name="borrowerId">The borrower id or user name</param>
        /// <returns>Transform data object</returns>
        [HttpGet("application/{applicationId}/borrower/{borrowerId}")]
        [ProducesResponseType(typeof(ITransformDataResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllSectionTransformData(string applicationId, string borrowerId)
        {
            Logger.Debug("Started Get Questionnaire all section data..");
            try
            {
                borrowerId = StringExtensions.ConvertToDocittCaseSensitiveType(borrowerId);
                var result = await AdapterService.GetAllSectionTransformData(applicationId, borrowerId);
                Logger.Debug("Ended Get Questionnaire all section data...");
                return Ok(result);
            }
            catch(DataConsistencyExtractException dc)
            {
                Logger.Error(dc.Message, dc);
                return ErrorResult.InternalServerError(dc.Message);
            }
            catch (System.Exception ex)
            {                
                Logger.Error(ex.Message, ex);
                return ErrorResult.InternalServerError("Server error. Try later.");
            }
        }
    }
}