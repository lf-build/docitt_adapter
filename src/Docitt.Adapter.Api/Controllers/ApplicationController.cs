﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Docitt.Adapter.Api.Controllers
{
    /// <summary>
    /// Represents application controller class.
    /// </summary>
    public class ApplicationController : ExtendedController
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationController"/> class.
        /// </summary>
        /// <param name="transformApplicationService"></param>
        /// <param name="logger"></param>
        public ApplicationController(ITransformApplicationService transformApplicationService, ILogger logger) : base(logger)
        {
            TransformApplicationService = transformApplicationService;
        }

        #endregion Constructor

        #region Variable Declaration

        /// <summary>
        /// Gets ApplicationService
        /// </summary>
        private ITransformApplicationService TransformApplicationService { get; }

        #endregion Variable Declaration

        #region Methods

        /// <summary>
        /// GetLoanTransformData
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/application/{applicationId}/loan")]
        public async Task<IActionResult> GetLoanTransformData(string applicationId)
        {
            try
            {
                Logger.Info("Starting the GetLoanTransformData Method...");
                var result = await TransformApplicationService.GetLoanTransformData(applicationId);

                return Ok(new
                {
                    TenantId = result.TenantId,
                    ApplicationId = applicationId,
                    OriginalData = result.OriginalData,
                    TransformedData = result.TransformedData
                });
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetLoanTransformData Method raised an error: {ex}",ex);
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// GetLoanTransformData
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/application/{applicationId}/isloan")]
        public async Task<IActionResult> CheckIsApplicationSubmitted(string applicationId)
        {
            try
            {
                Logger.Info("Starting the CheckIsApplicationSubmitted Method...");
                return Ok(await TransformApplicationService.CheckIsApplicationSubmitted(applicationId));
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetLoanTransformData Method raised an error: {ex}",ex);
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// GetConditionsTransformData
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/application/{applicationId}/conditions")]
        public async Task<IActionResult> GetConditionsTransformData(string applicationId)
        {
            try
            {
                Logger.Info("Starting the GetConditionsTransformData Method...");
                var result = await TransformApplicationService.GetConditionTransformData(applicationId);

                return Ok(new
                {
                    TenantId = result.TenantId,
                    ApplicationId = applicationId,
                    OriginalData = result.OriginalData,
                    TransformedData = result.TransformedData
                });
            }
            catch (NotFoundException ex)
            {
                return ErrorResult.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetConditionsTransformData Method raised an error: {ex}",ex);
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// GetConditionDocumentsTransformData
        /// </summary>
        /// <param name="applicationId">applicationId</param>
        /// <param name="conditionId">conditionId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("/application/{applicationId}/condition/{conditionId}/document")]
        public async Task<IActionResult> GetConditionDocumentsTransformData(string applicationId, string conditionId)
        {
            try
            {
                Logger.Info("Starting the GetConditionDocumentsTransformData Method...");
                var result = await TransformApplicationService.GetConditionDocumentsTransformData(applicationId, conditionId);

                return Ok(new
                {
                    TenantId = result.TenantId,
                    ApplicationId = applicationId,
                    ConditionId = conditionId,
                    OriginalData = result.OriginalData,
                    TransformedData = result.TransformedData
                });
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetConditionDocumentsTransformData Method raised an error: {ex}",ex);
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// GetConditionDocumentsTransformData
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/application/{applicationId}/documents")]
        public Task<IActionResult> GetApplicationDocumentsTransformData(string applicationId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    Logger.Info("Starting the GetApplicationDocumentsTransformData Method...");
                    var result = await TransformApplicationService.GetApplicationDocumentsTransformData(applicationId);

                    return Ok(new
                    {
                        TenantId = result.TenantId,
                        ApplicationId = applicationId,
                        OriginalData = result.OriginalData,
                        TransformedData = result.TransformedData
                    });
                }
                catch (Exception ex)
                {
                    Logger.Error($"The method GetApplicationDocumentsTransformData Method raised an error: {ex}",ex);
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetLoanTransformData
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/application/{applicationId}")]
        public async Task<IActionResult> GetApplicationTransformData(string applicationId)
        {
            try
            {
                Logger.Info("Starting the GetApplicationTransformData Method...");
                var result = await TransformApplicationService.GetApplicationTransformData(applicationId);

                return Ok(new
                {
                    TenantId = result.TenantId,
                    ApplicationId = applicationId,
                    OriginalData = result.OriginalData,
                    TransformedData = result.TransformedData
                });
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetApplicationTransformData Method raised an error: {ex}",ex);
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        #endregion Methods
    }
}