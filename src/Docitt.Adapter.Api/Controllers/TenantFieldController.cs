﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Adapter.Api.Controllers
{
    /// <summary>
    /// Represents tenant field controller class.
    /// </summary>
    public class TenantFieldController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="adapterService"></param>
        public TenantFieldController(ILogger logger, IAdapterService adapterService):base(logger)
        {
            AdapterService = adapterService;
        }     

        /// <summary>
        /// AdapterService
        /// </summary>
        private IAdapterService AdapterService { get; }

        /// <summary>
        /// NoContentResult
        /// </summary>
        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        /// <summary>
        /// GetTenantField
        /// </summary>
        /// <returns></returns>
        [HttpGet("tenant/fields")]
        [ProducesResponseType(typeof(List<ITenantField>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetTenantField()
        {
            try
            {
                Logger.Debug("Started GetTenantField...");

                var result = await AdapterService.GetTenantField();
                Logger.Debug("Ended GetTenantField...");
                return Ok(result);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// GetTenantField
        /// </summary>
        /// <param name="fieldname"></param>
        /// <returns></returns>
        [HttpGet("tenant/fields/{fieldname}")]
        [ProducesResponseType(typeof(List<ITenantField>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetTenantField(string fieldname)
        {
            try
            {
                Logger.Debug("Started GetTenantField(fieldName)...");

                var result = await AdapterService.GetTenantField(fieldname);
                Logger.Debug("Ended GetTenantField(fieldName)...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// AddTenantField
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        [HttpPost("tenant/field")]
        [ProducesResponseType(typeof(ITenantField), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddTenantField([FromBody]TenantField field)
        {
            try
            {
                Logger.Debug("Started AddTenantField...");

                var result = await AdapterService.AddTenantField(field);
                Logger.Debug("Ended AddTenantField...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {                
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// AddTenantFieldBulk
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        [HttpPost("tenant/field/bulk")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddTenantFieldBulk([FromBody]List<TenantField> field)
        {
            try
            {
                Logger.Debug("Started AddTenantFieldBulk...");

                foreach (var e in field)
                    await AdapterService.AddTenantField(e);
                Logger.Debug("Ended AddTenantFieldBulk...");
                return NoContentResult;
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// GetTenantFieldDetail
        /// </summary>
        /// <returns></returns>
        [HttpGet("tenant/field/all")]
        [ProducesResponseType(typeof(List<ITenantFieldCustom>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetTenantFieldDetail()
        {
            try
            {
                Logger.Debug("Started GetTenantFieldDetail...");

                var result = await AdapterService.GetTenantFieldDetail(string.Empty);
                Logger.Debug("Ended GetTenantFieldDetail...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {               
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// GetTenantFieldDetail
        /// </summary>
        /// <param name="fieldname"></param>
        /// <returns></returns>
        [HttpGet("tenant/field/{fieldname}/all")]
        [ProducesResponseType(typeof(List<ITenantFieldCustom>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetTenantFieldDetail(string fieldname)
        {
            try
            {
                Logger.Debug("Started GetTenantFieldDetail(fieldname)...");

                var result = await AdapterService.GetTenantFieldDetail(fieldname);
                Logger.Debug("Ended GetTenantFieldDetail(fieldname)...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// GetEnumFieldDetail
        /// </summary>

        [HttpGet("tenant/fieldenums/all")]
        [ProducesResponseType(typeof(ITenantFieldEnumCustom), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetEnumFieldDetail()
        {
            try
            {
                Logger.Debug("Started GetEnumFieldDetail(fieldname)...");
                var result = await AdapterService.GetEnumFieldDetail();
                Logger.Debug("Ended GetEnumFieldDetail(fieldname)...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// GetTenantFieldsByUpdateAfter
        /// </summary>
        /// <param name="updateAfterDate"></param>
        /// <returns></returns>
        [HttpGet("tenant/field/{updateAfterDate:datetime}/updateafter")]
        [ProducesResponseType(typeof(List<ITenantField>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetTenantFieldsByUpdateAfter(DateTime updateAfterDate)
        {
            try
            {
                Logger.Debug("Started GetTenantFieldsByUpdateAfter...");

                var result = await AdapterService.GetTenantFieldsByUpdateAfter(updateAfterDate);
                Logger.Debug("Ended GetTenantFieldsByUpdateAfter...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// UpdateTenantField
        /// </summary>
        /// <param name="fieldname"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPut("tenant/field/{fieldname}")]
        [ProducesResponseType(typeof(ITenantField), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateTenantField(string fieldname, [FromBody]TenantField data)
        {
            try
            {
                Logger.Debug("Started UpdateTenantField...");
                var result = await AdapterService.UpdateTenantField(fieldname, data);
                Logger.Debug("Ended UpdateTenantField...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// DeleteTenantField
        /// </summary>
        /// <param name="fieldname"></param>
        /// <returns></returns>
        [HttpDelete("tenant/field/{fieldname}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteTenantField(string fieldname)
        {
            try
            {
                Logger.Debug("Started DeleteTenantField...");

                var result = await AdapterService.DeleteTenantField(fieldname);
                Logger.Debug("Ended DeleteTenantField...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }
    }
}