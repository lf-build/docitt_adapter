﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Adapter.Api.Controllers
{
    /// <summary>
    /// Represents master enum controller.
    /// </summary>
    public class MasterEnumController : ExtendedController
    {
        /// <summary>
        /// Represetns constructor class.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="adapterService"></param>
        public MasterEnumController(ILogger logger, IAdapterService adapterService):base(logger)
        {
            AdapterService = adapterService;
        }

        /// <summary>
        /// AdapterService
        /// </summary>
        private IAdapterService AdapterService { get; }

        /// <summary>
        /// NoContentResult
        /// </summary>
        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        /// <summary>
        /// GetMasterEnum
        /// </summary>
        /// <returns></returns>
        [HttpGet("master/enums")]
        [ProducesResponseType(typeof(List<IMasterEnum>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetMasterEnum()
        {
            try
            {
                Logger.Debug("Started GetMasterEnum...");

                var result = await AdapterService.GetMasterEnum();
                Logger.Debug("Ended GetMasterEnum...");
                return Ok(result);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// GetMasterEnum
        /// </summary>
        /// <param name="enumname"></param>
        /// <returns></returns>
        [HttpGet("master/enums/{enumname}")]
        [ProducesResponseType(typeof(List<IMasterEnum>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetMasterEnum(string enumname)
        {
            try
            {
                Logger.Debug("Started GetMasterEnum(enumname)...");
                var result = await AdapterService.GetMasterEnum(enumname);
                Logger.Debug("Ended GetMasterEnum(enumname)...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// GetMasterEnumsByFieldName
        /// </summary>
        /// <param name="fieldname"></param>
        /// <returns></returns>
        [HttpGet("master/field/{fieldname}/enums")]
        [ProducesResponseType(typeof(List<IMasterEnum>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetMasterEnumsByFieldName(string fieldname)
        {
            try
            {
                Logger.Debug("Started GetMasterEnumsByFieldName(fieldname)...");

                var result = await AdapterService.GetMasterEnumsByFieldName(fieldname);
                Logger.Debug("Ended GetMasterEnum(fieldname)...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// GetMasterEnumsByUpdateAfter
        /// </summary>
        /// <param name="updateAfterDate"></param>
        /// <returns></returns>
        [HttpGet("master/enum/{updateAfterDate:datetime}/updateafter")]
        [ProducesResponseType(typeof(List<IMasterEnum>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetMasterEnumsByUpdateAfter(DateTime updateAfterDate)
        {
            try
            {
                Logger.Debug("Started GetMasterEnumsByUpdateAfter...");

                var result = await AdapterService.GetMasterEnumsByUpdateAfter(updateAfterDate);
                Logger.Debug("Ended GetMasterEnumsByUpdateAfter...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// AddMasterEnum
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost("master/enum")]
        [ProducesResponseType(typeof(IMasterEnum), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddMasterEnum([FromBody]MasterEnum data)
        {
            try
            {
                Logger.Debug("Started AddMasterEnum...");

                var result = await AdapterService.AddMasterEnum(data);
                Logger.Debug("Ended AddMasterEnum...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// AddMasterEnumBulk
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost("master/enum/bulk")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddMasterEnumBulk([FromBody]List<MasterEnum> data)
        {
            try
            {
                Logger.Debug("Started AddMasterEnumBulk...");

                foreach (var e in data)
                    await AdapterService.AddMasterEnum(e);
                Logger.Debug("Ended AddMasterEnumBulk...");
                return NoContentResult;
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// UpdateMasterEnum
        /// </summary>
        /// <param name="enumname"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPut("master/enum/{enumname}")]
        [ProducesResponseType(typeof(IMasterEnum), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateMasterEnum(string enumname, [FromBody]MasterEnum data)
        {
            try
            {
                Logger.Debug("Started UpdateMasterEnum...");
                var result = await AdapterService.UpdateMasterEnum(enumname, data);
                Logger.Debug("Ended UpdateMasterEnum...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// DeleteMasterEnum
        /// </summary>
        /// <param name="enumname"></param>
        /// <returns></returns>
        [HttpDelete("master/enum/{enumname}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteMasterEnum(string enumname)
        {
            try
            {
                Logger.Debug("Started DeleteMasterEnum...");

                var result = await AdapterService.DeleteMasterEnum(enumname);
                Logger.Debug("Ended DeleteMasterEnum...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {                
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }
    }
}