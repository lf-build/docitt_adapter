﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Adapter.Api.Controllers
{
    /// <summary>
    /// Represents tenant enum controller class.
    /// </summary>
    public class TenantEnumController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="adapterService"></param>
        public TenantEnumController(ILogger logger, IAdapterService adapterService):base(logger)
        {
            AdapterService = adapterService;
        }
        
        /// <summary>
        /// AdapterService
        /// </summary>
        private IAdapterService AdapterService { get; }

        /// <summary>
        /// NoContentResult
        /// </summary>
        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        /// <summary>
        /// GetTenantEnum
        /// </summary>
        /// <returns></returns>
        [HttpGet("tenant/enums")]
        [ProducesResponseType(typeof(List<ITenantEnum>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetTenantEnum()
        {
            try
            {
                Logger.Debug("Started GetTenantEnum...");
                var result = await AdapterService.GetTenantEnum();
                Logger.Debug("Ended GetTenantEnum...");
                return Ok(result);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// GetTenantEnum
        /// </summary>
        /// <param name="enumname"></param>
        /// <returns></returns>
        [HttpGet("tenant/enums/{enumname}")]
        [ProducesResponseType(typeof(List<ITenantEnum>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetTenantEnum(string enumname)
        {
            try
            {
                Logger.Debug("Started GetTenantEnum(enumname)...");

                var result = await AdapterService.GetTenantEnum(enumname);
                Logger.Debug("Ended GetTenantEnum(enumname)...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// GetTenantEnumsByFieldName
        /// </summary>
        /// <param name="fieldname"></param>
        /// <returns></returns>
        [HttpGet("tenant/field/{fieldname}/enums")]
        [ProducesResponseType(typeof(List<ITenantEnum>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetTenantEnumsByFieldName(string fieldname)
        {
            try
            {
                Logger.Debug("Started GetTenantEnumsByFieldName(fieldname)...");

                var result = await AdapterService.GetTenantEnumsByFieldName(fieldname);
                Logger.Debug("Ended GetTenantEnumsByFieldName(fieldname)...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {        
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// GetTenantEnumsByUpdateAfter
        /// </summary>
        /// <param name="updateAfterDate"></param>
        /// <returns></returns>
        [HttpGet("tenant/enum/{updateAfterDate:datetime}/updateafter")]
        [ProducesResponseType(typeof(List<ITenantEnum>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetTenantEnumsByUpdateAfter(DateTime updateAfterDate)
        {
            try
            {
                Logger.Debug("Started GetTenantEnumsByUpdateAfter...");

                var result = await AdapterService.GetTenantEnumsByUpdateAfter(updateAfterDate);
                Logger.Debug("Ended GetTenantEnumsByUpdateAfter...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {                
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// AddTenantEnum
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost("tenant/enum")]
        [ProducesResponseType(typeof(ITenantEnum), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddTenantEnum([FromBody]TenantEnum data)
        {
            try
            {
                Logger.Debug("Started AddTenantEnum...");

                var result = await AdapterService.AddTenantEnum(data);
                Logger.Debug("Ended AddTenantEnum...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// AddTenantEnumBulk
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost("tenant/enum/bulk")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddTenantEnumBulk([FromBody]List<TenantEnum> data)
        {
            try
            {
                Logger.Debug("Started AddTenantEnumBulk...");

                foreach (var e in data)
                    await AdapterService.AddTenantEnum(e);
                Logger.Debug("Ended AddTenantEnumBulk...");
                return NoContentResult;
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// UpdateTenantEnum
        /// </summary>
        /// <param name="enumname"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPut("tenant/enum/{enumname}")]
        [ProducesResponseType(typeof(ITenantEnum), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateTenantEnum(string enumname, [FromBody]TenantEnum data)
        {
            try
            {
                Logger.Debug("Started UpdateTenantEnum...");
                var result = await AdapterService.UpdateTenantEnum(enumname, data);
                Logger.Debug("Ended UpdateTenantEnum...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// DeleteTenantEnum
        /// </summary>
        /// <param name="enumname"></param>
        /// <returns></returns>
        [HttpDelete("tenant/enum/{enumname}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteTenantEnum(string enumname)
        {
            try
            {
                Logger.Debug("Started DeleteTenantEnum...");

                var result = await AdapterService.DeleteTenantEnum(enumname);
                Logger.Debug("Ended DeleteTenantEnum...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }
    }
}