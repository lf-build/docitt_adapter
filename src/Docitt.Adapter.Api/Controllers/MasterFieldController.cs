﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;

#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else

using Microsoft.AspNet.Mvc;

#endif

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Adapter.Api.Controllers
{
    /// <summary>
    /// Represents master field controller.
    /// </summary>
    public class MasterFieldController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="adapterService"></param>
        public MasterFieldController(ILogger logger, IAdapterService adapterService):base(logger)
        {
            AdapterService = adapterService;
        }   

        /// <summary>
        /// AdapterService
        /// </summary>
        private IAdapterService AdapterService { get; }

        /// <summary>
        /// NoContentResult
        /// </summary>
        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        /// <summary>
        /// GetMasterField
        /// </summary>
        /// <returns></returns>
        [HttpGet("master/fields")]
        [ProducesResponseType(typeof(List<IMasterField>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetMasterField()
        {
            try
            {
                Logger.Debug("Started GetMasterField...");

                var result = await AdapterService.GetMasterField();
                Logger.Debug("Ended GetMasterField...");
                return Ok(result);
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// GetMasterField
        /// </summary>
        /// <param name="fieldname"></param>
        /// <returns></returns>

        [HttpGet("master/fields/{fieldname}")]
        [ProducesResponseType(typeof(List<IMasterField>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetMasterField(string fieldname)
        {
            try
            {
                Logger.Debug("Inside GetMasterField(fieldname)...");
                var result = await AdapterService.GetMasterField(fieldname);
                Logger.Debug("Ended GetMasterField(fieldname)...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// GetMasterFieldsByUpdateAfter
        /// </summary>
        /// <param name="updateAfterDate"></param>
        /// <returns></returns>
        [HttpGet("master/field/{updateAfterDate:datetime}/updateafter")]
        [ProducesResponseType(typeof(List<IMasterField>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetMasterFieldsByUpdateAfter(DateTime updateAfterDate)
        {
            try
            {
                Logger.Debug("Started GetMasterFieldsByUpdateAfter...");

                var result = await AdapterService.GetMasterFieldsByUpdateAfter(updateAfterDate);
                Logger.Debug("Ended GetMasterFieldsByUpdateAfter...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// AddMasterField
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        [HttpPost("master/field")]
        [ProducesResponseType(typeof(IMasterField), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddMasterField([FromBody]MasterField field)
        {
            try
            {
                Logger.Info("Started AddMasterField...");

                var result = await AdapterService.AddMasterField(field);
                Logger.Debug("Ended AddMasterField...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// AddMasterFieldBulk
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        [HttpPost("master/field/bulk")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddMasterFieldBulk([FromBody]List<MasterField> field)
        {
            try
            {
                Logger.Info("Started AddMasterFieldBulk...");

                foreach (var e in field)
                    await AdapterService.AddMasterField(e);
                Logger.Debug("Ended AddMasterFieldBulk...");
                return NoContentResult;
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// UpdateMasterField
        /// </summary>
        /// <param name="fieldname"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPut("master/field/{fieldname}")]
        [ProducesResponseType(typeof(IMasterField), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateMasterField(string fieldname, [FromBody]MasterField data)
        {
            try
            {
                Logger.Debug("Started UpdateMasterField...");
                var result = await AdapterService.UpdateMasterField(fieldname, data);
                Logger.Debug("Ended UpdateMasterField...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// DeleteMasterField
        /// </summary>
        /// <param name="fieldname"></param>
        /// <returns></returns>
        [HttpDelete("master/field/{fieldname}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteMasterField(string fieldname)
        {
            try
            {
                Logger.Debug("Started DeleteMasterField...");

                var result = await AdapterService.DeleteMasterField(fieldname);
                Logger.Debug("Ended DeleteMasterField...");
                return Ok(result);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }
    }
}