﻿#if DOTNET2
using System;
using Microsoft.AspNetCore.Hosting;

namespace Docitt.Adapter.Api
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseStartup<Startup>()
                .Start("http://*:5000");

            using (host)
            {
                Console.WriteLine("Use Ctrl-C to shutdown the host...");
                //Console.WriteLine("Localhost:5000");
                host.WaitForShutdown();
            }
        }
    }
}
#endif